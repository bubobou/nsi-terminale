---
hide:
  - footer
---

# Modèle de données

En classe de de première vous avez eu l'occasion de travailler sur des données structurées 
en les stockant dans des fichiers au format CSV.
Même si cette méthode de stockage de l'information peut s'avérer pratique dans certains cas précis, 
elle atteint vite des limites lorsque la taille et le nombres de tables augmentent et des recherches complexes 
deviennent rapidement fastidieuses à programmer.


!!! example "Exemple de la bibliothèque/médiathèque"

    Si on imagine le fonctionnement d'une bibliothèque, on réalise que la gestion des personnes, des objets, 
    mais aussi des liens qui les relient, est complexe.

      - il faut lister les personnes, les objets et les relations (usager, livre, emprunt, employé, etc.)
      - pour chaque élément de la liste, il faut choisir les données pertinentes et donc procéder à une 
       approximation des données : c'est la modélisation

    Lors du fonctionnement du système, il faut fournir un certains nombres de services :

      - garantir l'absence d'erreur
      - permettre l'ajout/suppression de données
      - gérer les droits (car un usager ne doit pas avoir les même possibilités qu'un employé), etc ...

    C'est dans ce but qu'ont été mis en place les systèmes de gestion de bases de données.

## 1. Principes du modèle relationnel

Le modèle relationnel a été inventé par **Edgar Cobb**, ingénieur
travaillant pour IBM, dans les années 1970.

Son travail théorique (mathématique) a prouvé la forte cohérence de son
modèle ainsi que sa puissance.

L'implémentation de ce modèle quelques années plus tard a donné lieu à
un outil robuste et efficace qui a dominé le monde professionnel à
partir des années 1980.

**Les bases de données sont partout** : la gestion d'une entreprise
(salariés, payes, gestion, comptabilité, etc.), des produits (stocks,
entrées, sorties, valeurs, etc.), des clients (paniers, cartes
fidélités, etc.) ou encore actuellement des catalogues (photos, e-mails,
musiques, etc.) ou des réseaux sociaux.

!!! info "Le modèle relationnel est une façon de représenter et de manipuler des données"

      Ce modèle se fonde sur quelques principes essentiels

      -   une assise mathématique forte sur la théorie des ensembles
      -   une implémentation informatique très proche des mathématiques
      -   une formulation des énoncés sous forme de termes **logiques**
      -   une ensemble d'opérations élémentaires que l'on peut enchaîner pour créer des requêtes complexes

      Dans ce modèle, un objet (ou **entité**) est représenté par un *n-uplets* de valeurs et 
      les collections d'objets par des ensembles de *n-uplets*.

!!! warning "Modèle de données, structure de données"

    Attention, le chapitre *modèle de données* n'est pas lié au chapitre *structure de données* :

    - structure de données : s'intéresse à la façon d'organiser les 
     données en machine (tableaux, piles, files, objets, etc.)
  
    - modèle de données : s'intéresse à l'information contenue dans ces données. 
      L'implémentation machine est complètement occultée par le SGBD.

    Ce sont donc deux niveaux d'**abstractions** différents. 
    Le modèle de donnée est d'un niveau plus élevé.



!!! example "Exemple d'entité"

    Un livre emprunté dans une bibliothèque peut être représenté par le quintuplet suivant :

    ```
    ("Introduction à l'algorithmique", "Thomas Cormen", "Dunod", "01/12/1994", "2-10-001933-3")
    ```

L'ensemble des livres de la bibliothèque peut alors être représenté par un ensemble *Collection*.
Un tel ensemble est appelé une **relation**.


!!! example "Exemple de relation"

    ```
    Collection = {
      ("Introduction à l'algorithmique", "Thomas Cormen", "Dunod", 01/12/1994, "2-10-001933-3"), 
      ("Python pour le data scientist", "Emmanuel Jakobowicz", "Dunod", 03/03/2021, "978-2-10-081224-0 "), 
      ("Astérix Tome 39", "Didier Conrad", "Albert René (Editions)", 21/10/2021, "9782864973492"),
     
    ...

    }
    ```

Chaque relation se conforme à un **schéma** qui décrit chaque **attribut** (ou composante) des n-uplets.
En reprenant l'exemple précédent, les entités de la relation *Collection* possèdent les attributs suivant :

- titre : chaîne de caractère (`String`)
- auteur : chaîne de caractères
- éditeur : chaîne de caractères
- date de publication : `Date`
- isbn : chaîne de caractères (un isbn contient des tirets)

Un ensemble de relations constitue une **base de donnée**.

À partir de l'exemple que nous avons utilisé nous aurions pu aussi décrire la relation listant les usagers 
de la bibliothèques ou encore celle listant les emprunts.

## 2. Modélisation relationnelle des données 

### Principes généraux

Pour modéliser correctement des données il faut veiller à :

 - identifier les entités que l'on souhaite décrire (objets, personnes, actions, etc.), 
  ainsi que leurs attributs;
 - établir les schémas permettant de modéliser les ensembles d'entités, en veillant à choisir 
  des domaines pertinents pour leurs attributs;
 - définir les contraintes de la base de données afin que celle-ci reste cohérente.

### Contraintes d'intégrité

La **cohérence** des données au sein d'une base est assurée grace à des 
contraintes d'intégrité que celles-ci doivent impérativement respecter. 

!!! info "Les catégorie de contraintes d'intégrité"
    === "Contraintes d'entité"

        Chaque élément d'une relation doit être unique et doit pouvoir être identifié sans ambiguïté. 
        Dans l'exemple de la relation *Collection* il est tout a fait possible d'avoir deux ouvrages 
        portant le même titre, ou même plusieurs fois le même ouvrage.
        Pour éviter toute confusion il faut s'assurer que chaque entité d'une relation puisse être identifiée de façon unique 
        grâce à un attribut ou un ensemble d'attribut que l'on appelle la **clé primaire** de la relation.

        Pour la relation *Collection* il faudrait ajouter un attribut "numéro de document" (dont le domaine peut être de type `Int`), 
        le schéma serait alors :

        *Collection*(*titre* `String`, *auteur* `String`, *éditeur* `String`, *publication* `Date`, *isbn* `String`, ^^*numéro* `Int`^^)

        On indique une clé primaire dans un schéma en la soulignant.


    === "**Contraintes de références**"

        Au sein d'une base de données, les clés primaires d'une relation peuvent aussi servir de référence dans 
        une autre relation, elles y font alors office de *clé étrangère*.

        Dans l'exemple de la bibliothèque si l'on considère la relation des emprunts celle-ci devra contenir, 
        outre les dates d'emprunt et de retour, le numéro de l'ouvrage emprunté , ainsi que l'identifiant de 
        l'emprunteur. 

        Le numéro de l'ouvrage et le numéro sont des clés primaires respectivement dans les relations 
        *Collection* et *Usagers* et constituent des clés étrangères dans le schéma de la relation *Emprunts* :

        *Emprunts*(^^*identifiant* `String`^^, ^^*numéro* `String`^^, *date_emprunt* `Date` , *date_retour* `Date`)

        Cela créé des dépendances fortes entre les relations et permet de garantir la cohérence des bases.
        Cela empêche ainsi de supprimer des entités des relations *Collection* ou *Usagers* car cela supprimerait
        alors une clé primaire dans la base *Emprunts*, ce qui constitue une violation de contraint de référence.


    === "**Contraintes de domaines**"

        Chaque attribut d'une entité doit correspondre à un type de donnée bien choisi de façon 
        à représenter exactement les valeurs possibles d'un attribut (date, entier, chaîne de caractère, ...).


    === "**Contraintes utilisateurs**"

        Les contraintes de domaines ne sont pas toujours suffisantes pour exprimer les contraintes sur les attributs 
        d'une relation et l'utilisation que l'on souhaite en faire. Par exemple dans la relation *Emprunts*, la date 
        de retour ne peut être antérieure à la date d'emprunt. 


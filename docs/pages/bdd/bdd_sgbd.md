---
hide:
  - footer
---

# Systèmes de Gestion de Base de Données

Un Système de Gestion de Bases de Données (SGBD) regroupe des outils logiciels
permettant de gérer , manipuler, stocker et sécuriser des informations organisées 
dans une base de données.

## 1. Historique

Avant l'introduction par Edgar F. Codd du modèle relationnel 1970, Charles Bachman avait
déjà impulsé le développement de SGBD. Le premier SGBD commercial est
Oracle, commercialisé par la société Relational Software en 1979 (devenue
depuis Oracle Corporation). Les logiciels propriétaires tels qu'Oracle, mais
aussi DB/2 d'IBM ou Sybase (maintenant SAP ASE), sont pendant des
années la seule alternative viable pour les entreprises.

Avec le développement du Web au milieu des années 1990, le besoin de
solutions logicielles moins onéreuses et plus ouvertes augmente. En effet, les
SGBD propriétaires étaient non seulement coûteux, mais aussi conçus pour
fonctionner sur des serveurs aux architectures spécifiques, hors de portée des
particuliers, des associations et des petites entreprises. C'est dans ce contexte
que naissent des alternatives telles que MySQL (maintenant MariaDB) en
1995 puis PostgreSQL en 1996. Ces dernières sont devenues au cours des
25 dernières années des logiciels robustes, capables de concurrencer dans de
nombreux cas les alternatives propriétaires.

## 2. Rôle d'un SGBD

Un SGBD est un logiciel permettant : 

- de créer des bases de données, c'est-à-dire des ensembles de tables;
- de créer des tables en spécifiant leurs schémas;
- de spécifier des contraintes d'intégrité, telles que les clés primaires et
étrangères ou encore des contraintes de domaine ou des contraintes utilisateur;
- d'ajouter des données à des tables, mais uniquement si ces entités
respectent les contraintes;
- de mettre à jour ou de supprimer des données dans des tables et de
supprimer des tables;
- d'interroger les données grâce à des programmes écrits dans un langage
de requêtes;
- d'assurer la sûreté des données, par exemple en garantissant que même
en cas de problème matériel (coupure de courant, défaut sur un disque
dur, etc.) les données sont récupérables.

De plus, le SGBD devant permettre des accès simultanés aux données de la
part de plusieurs utilisateurs, il est souvent architecturé sur un modèle 
client-serveur.

## 3. Transactions

Au sein d'un SGBD l'ensemble des modification sur la base de données 
s'effectue au travers de *transactions*. Il s'agit d'une séquence d'instructions 
SQL (requêtes, mises à jour) qui forment un tout et doivent soit toutes réussir,
 soit toutes être annulées, afin de laisser la base dans un état cohérent. 

!!! tips "Transactions en SQL"

    En SQL pour déclarer qu'une suite d'ordres est une transaction, il suffit de la 
    faire précéder de `START TRANSACTION`. Et pour valider la transaction on utilise
    l'instruction `COMMIT`. L'instruction ROLLBACK permet de manuellement annuler 
    la transaction.
    
    Si une erreur se produit lors d'une transaction, alors toutes les tables
    sont remises dans leur état d'avant la transaction au moment du `COMMIT` ou
    du `ROLLBACK` (qui ont alors le même effet).

## 4. Propriétés ACID

Un SGBD garantie les propriétés d'*Atomicité*, de *Cohérence*, d'*Isolation*, 
et de *Durabilité* d'une base de données. Ces propriétés sont désignées par 
l'acronyme *ACID*.

**Atomicité** : Par ce terme, on désigne le fait qu'une transaction forme un 
"tout" indissociable et ne peut donc être exécutée partiellement. Soit la 
transaction est arrivée à son terme, soit elle a échoué, et toutes les 
modifications sont annulées pour restaurer la base de données dans l'état 
où elle était avant la transaction.

**Cohérence**: Les transactions doivent faire passer la base d'un état 
cohérent à un autre état cohérent. À l'issue d'une transaction, en particulier, 
toutes les contraintes d'intégrité doivent être vérifiées.

**Isolation**: Si deux transactions s'exécutent simultanément, alors leur 
exécution doit produire le même effet que si on les avait exécutées l'une
après l'autre.

**Durabilité** : Une transaction validée par un *COMMIT* est valide « pour de
bon ». Le système s'assure donc que, quels que soient les problèmes
logiciels ou matériels qui pourraient survenir (défaillance de disque
dur, panne de courant, etc.), les mises à jour d'une transaction validée
ne sont jamais perdues.

## 5. Interaction avec un programme

Un programme (simple) interagissant avec un SGBD effectue générale-
ment les actions suivantes :

1. Connexion au SGBD. C'est lors de cette phase que l'on spécifie où
se trouve le SGBD (par exemple en donnant son adresse IP), le nom
d'utilisateur et le mot de passe, ainsi que d'autres paramètres système.

2. Envoi d'ordres au SGDB. On crée (le plus souvent dans des chaînes
de caractères) des ordres SQL.

3. On récupère les données correspondant aux résultats dans des struc-
tures de données du langage (par exemple dans des tableaux Python).

4. On peut ensuite exécuter du code Python sur les données récupérées.

### 5.1 Connexion avec un SGBD

!!! example "Base de donnée SQlite et Python"

    Nous allons créer une base de données `sqlite` avec le module `sqlite3` de Python.

    ```python
    import sqlite3

    #Connexion
    connexion = sqlite3.connect('mynewbase.db')

    #Récupération d'un curseur
    c = connexion.cursor()

    # ---- début des instructions SQL

    #Création de la table
    c.execute("""
        CREATE TABLE IF NOT EXISTS bulletin(
        Nom TEXT,
        Prénom TEXT,
        Note INT);
        """)

    # ---- fin des instructions SQL

    #Validation
    connexion.commit()


    #Déconnexion
    connexion.close()
    ```


    - Le fichier ```mynewbase.db``` sera créé dans le même répertoire que le fichier source Python. Si fichier existe déjà, il est ouvert et peut être modifié.  
    - ```IF NOT EXISTS``` assure de ne pas écraser une table existante qui porterait le même nom. Si une telle table existe, elle n'est alors pas modifiée.  
    - La nouvelle table peut être ouverte avec ```DB Browser``` (ou directement dans `VScode` en utilisant un module approprié) pour vérifier sa structure et ses données.

    **Insertion d'enregistrements dans la table**

    Les morceaux de code ci-dessous sont à positionner entre les balises ```# ---- début des instructions SQL```  et ```# ---- fin des instructions SQL```.

    Insertion d'un enregistrement unique avec variable

    ```python  
    data = ('Simpson', 'Maggie', 2)
    c.execute('''INSERT INTO bulletin VALUES (?,?,?)''', data)
    ``` 

    Insertion de multiples enregistrements

    ```python
    lst_notes = [ ('Simpson', 'Lisa', 19), ('Muntz', 'Nelson', 4), ('Van Houten', 'Milhouse', 12) ]

    c.executemany('''INSERT INTO bulletin VALUES (?, ?, ?)''', lst_notes)
    ``` 
    Les différentes valeurs sont stockées au préalable dans une liste de tuples.

### 5.2 Lecture des enregistrements


```python
import sqlite3

#Connexion
connexion = sqlite3.connect('mynewbase.db')

#Récupération d'un curseur
c = connexion.cursor()

data = ('Simpson', )

c.execute("SELECT Prénom FROM Bulletin WHERE Nom = ?", data)
print(c.fetchall())  


#Déconnexion
connexion.close()
``` 

Ce code renvoie ```[('Homer',), ('Lisa',), ('Maggie',)]```, ou une liste vide s'il n'y a pas de résultat à la requête.

### 5. 3 Injection de SQL

L'injection SQL est une technique consistant à écrire du code SQL à un endroit qui n'est pas censé en recevoir.

![data/xkcd.png](https://imgs.xkcd.com/comics/exploits_of_a_mom.png)


- Créez un fichier contenant le code suivant :
```python
import sqlite3

#Connexion
connexion = sqlite3.connect('mabasecobaye.db')

#Récupération d'un curseur
c = connexion.cursor()

c.execute("""
    CREATE TABLE IF NOT EXISTS notes(
    Nom TEXT,
    Note INT);
    """)


while True :
    nom = input('Nom ? ')
    if nom in ['Q','q'] :
        break
    note = input('Note ? ')
    data = (nom, note)
    p = "INSERT INTO notes VALUES ('" + nom + "','" + note + "')"

    c.executescript(p)


#Validation
connexion.commit()


#Déconnexion
connexion.close()


``` 

- Exécutez ce fichier, rentrez quelques valeurs, quittez, et ouvrez dans ```DB Browser``` la table ```notes``` pour bien vérifier que vos valeurs ont bien été stockées.  
- Lancez à nouveau le fichier, en donnant ensuite comme nom la chaîne de caractères suivante : 
```g','3'); DROP TABLE notes;--```   
- Donnez une note quelconque (par exemple 12), quittez le programme... et allez observer l'état de la base de données. La table  ```notes``` n'existe plus !

**Explication** :  
La requête qui a été formulée est ```INSERT INTO notes VALUES ('g','3'); DROP TABLE notes;--','12')``` 

Dans un premier temps, le couple ```('g','3')``` a été inséré.  
Puis l'ordre a été donné de détruire la table ```notes```.  
Le reste du code (qui n'est pas correct) est ignoré car ```--``` est le symbole du commentaire en SQL (l'équivalent du # de Python).  

**Remarques** :  

Évidemment, ce code a été fait spécifiquement pour être vulnérable à l'injection SQL. Il suffit d'ailleurs de remplacer le ```c.executescript(p)``` par ```c.execute(p)``` pour que le code reste fonctionnel mais refuse l'injection SQL. 

Ceci dit, de nombreux serveurs sont encore attaqués par cette technique, au prix de manipulations bien sûr plus complexes que celles que nous venons de voir (vous pouvez par exemple regarder [ici](http://igm.univ-mlv.fr/~dr/XPOSE2011/injections_SQL/exploit.php)). 

Rappelons enfin que ce genre de pratiques est interdit sur un serveur qui ne vous appartient pas.


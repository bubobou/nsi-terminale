---
hide:
  - footer
---

# Requêtes SQL

Au chapitre précédent nous avons appris à créer des tables en respectant un schéma particulier, puis à y insérer
des données, tout cela en utilisant le langage SQL. Nous allons pouvoir maintenant découvrir comment formuler des
requêtes SQL afin d'extraire et manipuler ces données.  

Si l'on reprend l'exemple de la bibliothèque que nous suivons depuis le début, nous pourrions être amené à nous poser 
 les questions suivantes :

- Étant donné un code utilisateur, quels sont les livres empruntés par 
  l'utilisateur correspondant ?
- Quels sont les utilisateurs en retard, c'est-à-dire ceux dont la date de
retour est inférieure à une date donnée ?
- Quel est le nombre total de livres empruntés ?
- Comment mettre à jour des données d'utilisateur ?


## 1. Extraire des données

### 1.a Exemple de requête sur une table

Commençons par une requête simple. On considère la table livre, créée
par l'ordre suivant :

```SQL
    CREATE TABLE livre (titre TEXT NOT NULL,
                        editeur TEXT NOT NULL,
                        annee INT NOT NULL,
                        isbn TEXT PRIMARY KEY);
```

On souhaite trouver les titres de tous les livres publiés après 2005 dans la
base de données de la bibliothèque. Une telle requête peut s'écrire en SQL :

```SQL
    SELECT titre FROM livre WHERE annee >= 2005;
```

!!! example "Exemple de résultat de requête SELECT"

    L'IDE suivant contient déjà la base en mémoire, vous pouvez vous exercer à 
    y entrer la requête de sélection précédente.

     {!{ sqlide titre="" init="sql/livres.sql" sql="" espace="bdd2"}!}



Dans cette requête :  

- la partie `SELECT titre` indique qu'on ne veut renvoyer que les valeurs de l'attribut
   `titre` des lignes trouvées. 
- la partie `FROM livre` indique que la requête porte sur la table `livre`;
- la partie `WHERE`  indique que l'on ne sélectionne que les lignes de la table livre 
  pour lesquelles la valeur de l'attribut `annee` est plus grande que 2005;
  
### 1.b Précision sur l'utilisation de `WHERE`

L'expression se trouvant dans la partie `WHERE` doit être une **expression booléenne**.  

Elle peut être construite à partir :  

- d'opérateurs de comparaison «, <=, >, >=, = et <>l),  
- d'opérateurs arithmétiques (+, -, *, l, %),  
- de constantes,  
- de noms d'attributs, 
- d'opérateurs logiques (AND, OR et NOT),
- d'opérateurs spéciaux tels que l'opérateur de comparaison de textes.


!!! example "Exemple composition avec `WHERE`"

    On peut ainsi cumuler les opérateurs, ainsi si l'on souhaite connaître les titres de
    tous les livres publiés par les éditions Flammarion entre 2000 et 2020 :

    ```SQL
    SELECT titre FROM livre WHERE annee >= 2000 AND
                                  annee <= 2020 AND
                                  editeur = 'Flammarion';
    ```
    Vous pouvez vous exercer ci-dessous à écrire des requêtes de telle sorte :

     {!{ sqlide titre="" init="" sql="" espace="bdd2"}!}


!!! tip "Recherche approchée"
    Si l'utilisation de l'égalité « = » est appropriée ici, on pourrait vouloir
    faire une requête approchée. Par exemple, trouver les titres des livres qui
    contiennent le mot « Astérix» dans le titre. Une telle requête s'écrira :  

    ```SQL
    SELECT titre FROM livre WHERE titre LIKE '%Astérix%';
    ```

    Dans un motif, le symbole « % » est un joker et peut être substitué par n'importe 
    quelle chaîne. Le symbole « _ » quant à lui représente n'importe quel caractère.


### 1.c Précision sur l'utilisation de `SELECT`

#### Sélection générale

Lorsque l'on souhaite récupérer toutes les colonnes de la table dans le résultat
de la requête, on l'indique avec le caractère `*`.

!!! example "Exemple de sélection globale"
    ```SQL
    SELECT * FROM livre WHERE annee >= 2020;
    ```

#### Sélection explicite

Si l'on souhaite ne conserver qu'une partie des colonnes, il faut préciser
explicitement les attributs correspondants. On les liste alors immédiatement
après le mot clé `SELECT` :

!!! example "Exemple de sélection explicite"

    ```SQL
    SELECT titre, annee FROM livre WHERE annee >= 2020;
    ```

!!! tip "Renommage des colonnes"
    Il est possible de renommer les colonnes au moyen du mot clé `AS`.  
    Cela sera notamment utile lorsqu'on effectuera des opérations de jointures
    ou l'on risque de se retrouver avec plusieurs attributs portant le même nom.  
    
    ```SQL
    SELECT titre AS titre_avec_Asterix FROM livre WHERE titre LIKE '%Astérix%';
    ```

    {!{ sqlide titre="" init="" sql="" espace="bdd2"}!}


### 1.d Fonctions d'agrégation

Les fonctions d'agrégation permettent d'appliquer une fonction à l'ensemble des valeurs d'une colonne et de renvoyer le résultat comme une table ayant une seule case (une ligne et une colonne). 

Il est ainsi possible :

- d'obtenir le nombre de résultat avec `COUNT`,
- de faire la somme des valeurs d'une colonne avec `SUM`,
- de calculer la moyenne d'un colonne avec `AVG`,
- de trouver le minimum ou le maximum avec `MIN` et `MAX`.


!!! example "Exemple d'utilisation de `COUNT`"
    Si on souhaite savoir combien de livres contiennent la chaîne « Astérix » dans leur
    titre (plutôt que de renvoyer ces titres), on écrira la requête suivante:

    ```SQL
    SELECT COUNT(titre) AS total  FROM livre 
                                  WHERE titre LIKE '%Astérix%';
    ```

### 1.e Tri des résultats

Pour obtenir des résultats selon un ordre particulier, il faut utiliser le mots clés `ORDER BY`
à la fin de la requête en précisant la colonne sur laquelle on souhaite faire le tri puis le sens 
de classement :

- `ASC` : croissant (par défaut);
- `DESC` : décroissant.

!!! example "Exemple de tri par ordre décroissant"
    Pour obtenir les titres des livres par ordre d'année de parution décroissante :  

    ```SQL
        SELECT titre FROM livre ORDER BY annee DESC
    ```
Ce tri opère aussi pour les chaînes de caractères<!-- .slide: data-fullscreen -->

### 1.f Élimination des doublons

Si l'on souhaite connaître toutes les années dans
lesquelles un livre a été publié, on pourrait écrire la requête :  

```SQL
SELECT annee FROM livre;
```
On aura alors bien toutes les années, mais la même année peut apparaître plusieurs
fois ce qui n'est pas forcément souhaitable.

Pour retirer les doublons d'un résultat, il faut ajouter le mot clé `DISTINCT` après `SELECT`:  

```SQL
SELECT DISTINCT annee FROM livre;
```

## 2. Jointures

Les requêtes que nous avons vues nous permettent assez facilement de déterminer les livres qui ont été empruntés. 

```SQL
SELECT * FROM emprunt;
```

Cependant cette réponse affiche les `isbn` présents dans la table *emprunt*, or il serait plus
naturel de pouvoir afficher les titres de ces livres plutôt que leur `isbn`. Le problème est 
que les titres des livres sont présents uniquement dans la table livre. 

L'opération de jointure de deux tables apporte une réponse à ce problème.

!!! note "Jointure de deux tables"

    Étant données deux tables A et B, la jointure consiste à créer toutes combinaisons de lignes 
    de A et de B. D'un point de vue mathématique, on parle de *produit cartésien*.  

    Bien entendu l'intérêt pour nous sera d'effectuer une jointure pour les lignes ayant un attribut
     de même valeur.


Ici, on souhaiterait obtenir une « grande table » dont les colonnes sont celles de la table emprunt 
et celle de la table livre, en réunissant les lignes ayant le même `isbn`. Cela peut être fait au moyen 
de la directive `JOIN`.

```SQL
SELECT * FROM emprunt
         JOIN livre ON emprunt.isbn = livre.isbn;
```

{!{ sqlide titre="" init="" sql="" espace="bdd2"}!}

Cette requête crée la jointure des deux tables.  

Comme on peut le voir, toutes les colonnes des deux tables ont été recopiées dans la sortie. 
Chaque ligne est le résultat de la fusion de deux lignes ayant le même ISBN. Le choix de ces 
lignes est donné par la condition de jointure indiquée par le mot clé `ON`. La condition
indique au SGBD dans quel cas deux lignes doivent être fusionnées.  

Ici, on joint les lignes pour lesquelles les ISBN sont égaux. On écrit donc l'expression 
booléenne  `emprunt.isbn = livre.isbn`.

La notation `nom_de_table.attribut` permet de différencier entre deux attributs portant 
le même nom. Une bonne pratique consiste à préfixer les noms d'attributs par leur table 
dès que l'on utilise plus d'une table dans la requête. 

!!! example "La jointure peut être combinée avec les clauses `SELECT` et `WHERE`. "

    Par exemple, si on souhaite afficher uniquement les titres et les dates des livres empruntés qui sont à rendre avant le 6 mai 2020, on peut écrire la requête suivante :  

    ```SQL
    SELECT livre. titre, emprunt. retour
    FROM emprunt
    JOIN livre ON emprunt.isbn = livre.isbn
    WHERE emprunt. retour < '2020-05-06';
    ```
    Que vous pouvez tester et modifier ci-dessous :  

    {!{ sqlide titre="" init="" sql="" espace="bdd2"}!}


On n'est évidemment pas limité à une seule jointure. Si on souhaite afficher les noms et prénoms 
des utilisateurs ayant emprunté ces livres, il suffit de joindre la table usager, en rajoutant 
une nouvelle clause JOIN ON, cette fois sur le code_barre de l'usager.  

```SQL
SELECT usager. nom, usager. prenom,
livre. titre, emprunt. retour
FROM emprunt
JOIN livre ON emprunt.isbn = livre.isbn
JOIN usager ON usager. code_barre = emprunt. code_barre
WHERE emprunt. retour < '2020-05-06';
```

La requête devenant assez longue à écrire ne faut pas hésiter à utiliser le renommage des colonnes
au moyen du mot clé `AS` évoqué un peu plus haut.

On peut ainsi utiliser `u` pour désigner la table `usager`, `l` pour désigner la table `livre` et `e` pour désigner la table `emprunt` :

```SQL
SELECT u.nom, u.prenom, l.titre, e.retour
FROM emprunt AS e
JOIN livre AS l ON e.isbn = l.isbn
JOIN usager AS u ON u.code_barre = e.code_barre
WHERE e.retour < '2020-02-01';
```

## 3. Modifier des données

Il est bien entendu possible de modifier les données stockées dans un SGBD.  

Nous allons étudier les deux types de modifications principales que sont :  
- la suppression d'un ensemble de lignes,  
- la mise à jour de certains attributs d'un ensemble de lignes. 

### 3.a Suppression de lignes

L'ordre `DELETE FROM t WHERE c` permet de supprimer de la table `t`
toutes les lignes vérifiant la condition `c`. 

Dans l'exemple de notre médiathèque, supposons que l'utilisateur Philippe Dubois, dont le code_barre
est '`137332830764072`', ait rendu ses livres. Il faut supprimer de la table emprunt toutes les lignes 
pour lesquelles le code_barre vaut  `137332830764072`, ce qui donne l'ordre suivant:

```SQL
DELETE FROM emprunt WHERE code_barre = '137332830764072';
```

!!! danger "usage de DELETE sans clause WHERE"

    Attention, un ordre `DELETE` sans clause `WHERE` efface toutes les lignes de la table. 
    Il ne faut pas confondre `DELETE FROM t` et `DROP TABLE t` : la première opération vide 
    une table de son contenu, mais ne supprime pas la table, alors que la seconde opération détruit 
    la table (et ses données). La table ne peut alors plus être référencée.

!!! info "vérification des contraintes"
    Lors de requête de suppression le SGB veille bien normalement à ce que les contraintes de référence 
    soient vérifiées.  

    Si l'on essaye de supprimer une ligne d'une table alors qu'au moins un attribut sert de clé étrangère, 
    le système renvoie une erreur et la requête n'est pas exécutée.

    Il faut donc supprimer en premier les lignes dont les attributs sont déclarés comme clés étrangères 
    avant de supprimer celles contenant les clés primaires correspondantes.

!!! example "Exemple de requête de suppression violant une contrainte de référence"
    Par exemple si l'on essaye de supprimer un livre ayant pour titre 'Akira' :  

    ```SQL
        DELETE FROM livre WHERE titre = 'Akira'
    ```

    La réponse du SGBD devrait être.

    ```SQL
        ERROR: update or delete on table Il livre Il violates foreign
        key constraint "lI"auteur_de_isbn_fkey" on table "auteur_de"
    ```

    Remarque : ce n'est pas forcément le cas avec les IDE inclus dans cette page.

Si un SGBD rencontre une violation de contrainte au cours de l'exécution d'une requête, toutes les modifications déjà faites seront annulées et la table se trouvera dans l'état qu'elle avait avant 
la tentative d'exécution.


### 3.b Mise à jour de valeurs

Le second type de modification est la mise à jour. Elle consiste à rem-
placer certains attributs d'un ensemble de lignes par de nouvelles valeurs.
La syntaxe est la suivante :

```SQL
        UPDATE t SET a_1 = e_1, ..., SET a_n = e_n WHERE c
```

Par exemple, si l'utilisateur Philippe Dubois souhaite mettre à jour son adresse email, 
on écrit ceci:

```SQL
    UPDATE utilisateur SET email = 'phi.dubois@courriel.com'
    WHERE code_barre = '137332830764072';
```
Il est aussi possible d'utiliser les attributs dans une expression de mise à jour.

Supposons par exemple que la médiathèque soit fermée au mois d'avril. On souhaite que 
tous les emprunts dont la date de rendu était en avril soient prolongés de 30 jours.
La requête peut être saisie de la façon suivante :  

```SQL
    UPDATE emprunt SET retour = retour + 30
    WHERE retour >= '2020-04-01';
```
---
hide:
  - footer
---

# Bases de données relationnelles

Au chapitre précédent nous avons étudier le modèle relationnel qui est un modèle théorique permettant de 
raisonner sur les données et leurs relations. Le modèle physique qui va permettre d'interagir réellement
avec les données est le Système de Gestion de Base de Donnée (SGBD) qui va par l'intermédiaire d'un langage
 approprié va donner la possibilité aux utilisateurs de créer, consulter et manipuler des données.

Un SGBD relationnel est un SGBD utilisant le modèle relationnel pour la représentation des données. 
L'écrasante majorité des SGBD relationnels utilisent le langage SQL (Structured Query Language, langage 
de requête structuré). Ce dernier permet d'envoyer des ordres au SGBD.  

Les ordres peuvent être de deux natures:  

- Les mises à jour permettent la création de relations, l'ajout d'entité dans ces dernières, leur 
modification et leur suppression. 
  
- Les requêtes permettent de récupérer les données répondant à des critères particuliers.

!!! warning "Vocabulaire"

    Il est important de connaître et  différencier le vocabulaire utilisé pour la modélisation et 
    celui utilisé avec les bases de données. Nous allons utiliser les équivalences suivantes, même
    si elles ne sont pas tout à fait exactes.

    - relation = table,  
    - attribut = champ = colonne,  
    - tuple = n-uplet = ligne.

    Lors de la création d'une table il n'est par exemple pas obligatoire de spécifier une clé primaire, 
    ce qui induit alors la possibilité de doublons.


    
    

## 1. Créer des tables avec SQL


Le langage SQL, inspiré du modèle relationnel introduit par 
[E. Codd](https://fr.wikipedia.org/wiki/Edgar_Frank_Codd){target=”_blank”}
, permet de **créer des tables** en spécifiant leur nom, leurs attributs, les types de ces derniers et 
les contraintes associées à la table, c'est ce que nous étudierons dans ce chapitre.

Pour créer une table, la syntaxe de la commande est :  `SQL CREATE TABLE nom_de_la_table` , et la 
syntaxe d'un ordre `SQL CREATE TABLE` est la suivante:


```SQL
CREATE TABLE nom_de_la_table (attribut_1 domaine_1 contrainte_1,
                        ... ,
                        attribut_n, domaine_n, contrainte_n ,
                        contraintes_globales ,
                        ... ,
                        ) ;
```

Une définition d'attribut consiste en un **nom** d'attribut, un **type** d'attribut (ou *domaine*) qui 
sont tous les deux obligatoires et optionnellement des **contraintes** sur cet attribut. 

Ainsi pour créer les tables correspondant à l'exemple de la médiathèque proposé au chapitre précédent, 
on peut saisir les ordres suivants:

!!! example "Codes de création des tables"
    === "Usagers"
        ```sql
        CREATE TABLE usager ( nom VARCHAR(90) , prenom VARCHAR(90) ,
                              adresse VARCHAR(300) , cp VARCHAR(5) ,
                              ville VARCHAR(60) , email VARCHAR(60) ,
                              id_usager CHAR(15) PRIMARY KEY);
        ```
    === "Livres"
        ```sql
        CREATE TABLE  livre ( titre VARCHAR(300) ,
                              editeur VARCHAR(90) ,
                              annee INT,
                              isbn CHAR(14),
                              id_livre PRIMARY KEY);

        ```
    === "Emprunts"
        ```sql
        CREATE TABLE emprunt (id_usager CHAR(15),
                                REFERENCES usager(id_usager),
                              id_livre CHAR(14) PRIMARY KEY,
                                REFERENCES livre(id_livre),
                              retour DATE);

        ```

!!! warning "SQL vs SQLite"

    Le langage SQL a été créé en 1979 et normalisé en 1986. Il est utilisé dans la majorité des SGBD, 
    néanmoins l'installation de tels système est lourde et plus ou moins complexe, en plus d'être 
    soumise à des licences propriétaires.

    Nous utiliserons des systèmes de bases de données fonctionnant avec le langage SQLite qui 
    offre de moins de possibilités que SQL, mais qui a l'avantage d'être sous licence 
    publique et surtout d'être facilement intégrable dans un navigateur.

    SQLite n'est pas sensible à la casse : les commandes seront aussi bien interprétées si elles sont 
    écrites en minuscules ou en majuscules. Néanmoins dans un soucis de lisibilité et de bonne pratique 
    nous utiliserons uniquement les majuscules.

## 2. Types de données

Les domaines du modèle relationnel correspondent à des types de données dans le langage SQL.
Le standard SQL définit plus de types que ce que propose SQLite, nous allons en présenter les principaux
en indiquant pour chacun son équivalent en SQLite.

### 2.a. Types numériques

| Nom du type SQL   | description             | équivalent SQLite |
|-------------------|-------------------------|-------------------|
| `SMALLINT`          | entier 16 bits signé    | `INTEGER`           |
| `INT` ou `INTEGER`    | entier 32 bits signé    | `INTEGER`           |
| `BIGINT`            | entier 64 bits signé    | `INTEGER`           |
| `DECIMAL`(*t*, *f*) | décimal signé de *t* chiffres dont *f* après la virgule| |
| `REAL`              | flottant 32 bits        | `REAL` |
| `DOUBLE PRECISION`  | flottant 64 bits        | `REAL` |


### 2.b. Types textes

| Nom du type SQL   | description             | équivalent SQLite |
|-------------------|-------------------------|-------------------|
| `CHAR`(*n*) | Chaîne d'exactement *n* caractères | `TEXT` |
| `VARCHAR`(*n*) | Chaîne d'**au plus** *n* caractères | `TEXT` |
| `TEXT`  | Chaîne de taille quelconque  | `TEXT` |

!!! info "Remarques "
    Si l'on stocke une chaîne de taille inférieure à *n* dans un type `CHAR`, celle-ci sera 
    complétée  par des espaces.  
    
    Une chaîne peut être écrite sur plusieurs lignes.

### 2.b. Type booléen

En SQL le type booléen se déclare : `BOOLEAN` , il n'existe pas d'équivalent avec SQLite : les 
valeurs `TRUE` et `FALSE` sont reconnues et interprétées respectivement en `1` et  `0`.

### 2.c. Types dates

| Nom du type SQL   | description             |
|-------------------|-------------------------|
| DATE | une date au format 'AAAA-MM-JJ'|
| TIME | une heure au format 'hh: mm: ss'|
| TIMESTAMP | un instant (date et heure) au format 'AAAA-MM-JJ-hh: mm: ss' |

Là encore SQLite ne dispose pas de type spécifique pour les dates, mais il dispose de fonctions
qui seront capables de manipuler des dates qui seront enregistrées avec les différents types
 `TEXT`, `REAL` ou `INTEGER` .

???+ sql "À vous de jouer !"

    Vous pouvez écrire ci-dessous un code de création de table.  
    Pour visualiser la structure de votre table création, il faudra ajouter la ligne
    ```SQL
    SELECT * FROM sqlite_master WHERE TYPE ="table"
    ``` 
    (qu'on ne détaillera pas ici).

    Cliquer ensuite sur Exécuter

    {!{ sqlide titre="IDE SQLite" init="" sql="" espace="bdd1"}!}

   
 
## 3. Contraintes d'intégrités

Dans le chapitre précédent nous avons détaillé quatre types de contraintes d'intégrité,
nous allons ici les traduire en commandes SQL.

### 3.a. Contrainte d'entité

La contrainte d'intégrité est garantie par la présence de clés primaires, pour spécifier qu'un attribut 
est une clé primaire on l'indique par les mots clés `PRIMARY KEY`.

!!! example "Exemple"
    Ici c'est l'attribut `num` qui fait office de clé primaire.
    ```SQL
    CREATE TABLE eleve (num INT PRIMARY KEY, age INT, nom TEXT, prenom TEXT);
    ```
### 3.b. Contrainte de référence

Pour spécifier une clé étrangère, il faut utiliser le mot clé `REFERENCE` à la suite de l'attribut,
en indiquant la table et l'attribut de référence:

!!! example "Exemple"
    
    ```SQL
    CREATE TABLE parent (num INT PRIMARY KEY, nom TEXT, prenom TEXT, id_el INT REFERENCES eleve (num) );
    ```
    Ici c'est l'attribut `id_el` qui fait office de clé étrangère et l'on précise qu'il fait référence
    à l'attribut `num` de la table `eleve`.

### 3.c. Contrainte de domaines

Comme vous avez pu le constater, les contraintes de domaines sont indiquées en précisant le type de données
à la suite du nom de l'attribut. Même si SQLite est permissif au niveau de la casse on s'efforcera de déclarer
le domaine avec des majuscules.

### 3.d. Contraintes utilisateurs

Pour spécifier des contraintes utilisateurs on utilisera le mot clé CHECK, suivi d'une formule. 
Cette formule doit se trouver en fin de déclaration, avant la parenthèse fermante.

!!! example "Exemple"
    Par exemple il est souhaitable que l'âge d'un élève ne puisse être un nombre négatif :

    ```SQL
    CREATE TABLE eleve (num INT PRIMARY KEY,
                        age INT, 
                        nom TEXT, 
                        prenom TEXT,
                        CHECK (age >= 0));
    ```
Il peut être intéressant de spécifier qu'un attribut doive être obligatoirement renseigner et donc ne puisse pas
prendre la valeur `NULL` , dans ce cas on utilisera les mots clés `NOT NULL`. 

Il est aussi possible de préciser qu'un groupe d'attributs doive être unique, sans pour autant
en faire des clés primaire. Dans ce cas, le mot clé à utilisé est `UNIQUE`.


## 4. Supprimer une table

Lorsqu'une table est créée, son nom devient réservé, il n'est donc pas possible d'en créer une homonyme. 
Si on souhaite recréer la table, par exemple pour modifier son schéma, il faut d'abord 
supprimer celle portant le même nom. Cette suppression est réalisée par l'instruction `DROP TABLE`. 

Cette commande supprime aussi toutes les données stockées dans la table.

Pour éviter de violer une contrainte de référence, il n'est donc pas possible de supprimer 
une table si elle sert de référence pour une clé étrangère d'une autre table.

## 5. Insérer des valeurs dans une table

L'insertion de nouvelles valeurs dans une table s'effectue au moyen de l'ordre `INSERT INTO`.

On spécifie le nom de la table (ici `eleve`) suivi d'une suite de n-uplets,
chacun entre parenthèses. Chaque *n-uplet* représente une nouvelle ligne de
la table. Les valeurs des attributs sont supposés être **dans le même ordre** que
lors de l'instruction `CREATE TABLE`.  

Si on souhaite les passer dans un ordre différent,
on peut spécifier l'ordre des attributs avant le mot clé `VALUES` :  

!!! example "Exemple"

    ```SQL
    INSERT INTO eleve (prenom, num, nom, age) VALUES
                ('Jean', 23, 'Bombeur', 17);
    ```

!!! warning "Vérification des contraintes d'intégrité"
    Les contraintes d'intégrités sont vérifiées au moment  de l'insertion. 
    Une instruction `INSERT` violant ces contraintes conduira donc à une erreur 
    et les données correspondantes ne seront pas ajoutées à la table.

???+ sql "À vous de jouer ! (BIS)"

    Vous pouvez écrire ci-dessous un code de d'insertion dans une table.
    Cela peut être la table que vous avez créée dans l'IDE au-dessus. 

    Pour visualiser le contenu de votre table, il faudra ajouter la ligne
    ```SQL
    SELECT * FROM nom_de_table
    ``` 
    (qu'on ne détaillera pas ici).

    Cliquer ensuite sur Exécuter

    {!{ sqlide titre="IDE SQLite" init="" sql="" espace="bdd1"}!}

---
hide:
  - footer
---

# Gestion des processus et des ressources

## 1. Ordonnancement des processus

### 1.1 Exécution d'un programme

Lorsque l'on exécute un programme (par exemple en cliquant sur l'icône du fichier 
 ou en renseignant
son chemin dans un terminal), le système d'exploitation effectue les actions
suivantes :

1. le fichier contenant le programme (l'exécutable) est copié dans la mé-
moire RAM, à une certaine adresse a ;

2. le système d'exploitation écrit la valeur a dans le registre IP (instruc-
tion pointer).

Au prochain cycle d'horloge du processeur, ce dernier va alors lire l'instruc-
tion se trouvant à l'adresse a et l'exécuter. Une fois cela fait, il exécutera
ensuite la seconde instruction et ainsi de suite. On rappelle que l'exécution
d'une instruction se décompose elle-même en plusieurs sous-étapes effectuées
au sein du processeur: le chargement (récupérer l'instruction en mémoire),
le décodage (déterminer dans la suite d'octets chargés quelle instruction ils
encodent) et l'exécution proprement dite.

### 1.2 Interruptions

Si rien de plus n'est fait, alors la seule chose que l'on peut attendre, c'est que le 
programme s'exécute jusqu'à sa dernière instruction, puis rende la main au système
d'exploitation. Impossible alors de l'interrompre et donc aussi de pouvoir exécuter 
deux programmes en même temps. C'est pour cela que les systèmes d'exploitation
utilise le principe de l'interruption.

Une interruption est un signal envoyé au processeur lorsqu'un événement
se produit. Il existe plusieurs types d'interruptions. Certaines sont générées
par le matériel (par exemple, un disque dur signale qu'il a fini d'écrire des
octets, une carte réseau signale que des paquets de données arrivent, etc.).*

Lorsque le processeur reçoit une interruption, il interrompt son exécution à
la fin de l'instruction courante et exécute un programme se trouvant à une
adresse prédéfinie. Ce programme reçoit en argument une copie des valeurs
courante des registres, ainsi qu'un code numérique lui permettant de savoir
à quel type d'interruption il fait face. Ce programme spécial s'appelle **le
gestionnaire d'interruption**. 

Parmi les interruptions matérielles, on retrouve les **interruptions d 'horloge**. 
Le processeur génère de lui-même une interruption matérielle à intervalles de 
temps fixe. Ces interruptions d'horloges, alliées au gestionnaire d'interruption, 
sont les pièces essentielles permettant d'exécuter des programmes de façon concurrente.

### 1.3 Vocabulaire


**Exécutable** : un fichier binaire contenant des instructions machines directement
 exécutables par le processeur de la machine.

**Processus** : « programme en cours d'exécution ». Un processus est le phénomène 
dynamique qui correspond à l'exécution d'un programme particulier. Le système 
d'exploitation identifie généralement les processus par un numéro unique. 
Un processus est décrit par:

- l'ensemble de la mémoire allouée par le système pour l'exécution
de ce programme (ce qui inclut le code exécutable copié en mémoire et toutes 
les données manipulées par le programme, sur la pile ou dans le tas;  
- l'ensemble des ressources utilisées par le programme (fichiers ouverts, 
  connexions réseaux, etc.) ;  
- les valeurs stockées dans tous les registres du processeur.

**Thread** (ou tâche): exécution d'une suite d'instructions démarrée par un
processus. Deux *processus* sont l'exécution de deux programmes (par
exemple, un traitement de texte et un navigateur web). Deux *threads*
sont l'exécution concurrente de deux suites d'instructions d'un même
processus. La différence fondamentale entre *processus* et *thread* est que 
les processus ne partagent pas leur mémoire, alors que les threads, issus d'un même 
processus, peuvent accéder aux variables globales du programme et occupent le même 
espace en mémoire.

**Exécution concurrente** : deux processus ou tâches s'exécutent de manière 
concurrente si les intervalles de temps entre le début et la fin de leur exécution 
ont une partie commune.

**Exécution parallèle** : deux processus ou tâches s'exécutent en parallèle
s'ils s'exécutent au même instant. Pour que deux processus s'éxcutent
en parallèle, il faut donc plusieurs processeurs sur la machine.

### 1.4 Ordonnanceur

Le système d'exploitation peut configurer une horloge et le gestionnaire 
d'interruption pour « reprendre la main », c'est-à-dire exécuter du code 
qui lui est propre, à intervalles réguliers. Lorsqu'il s'exécute, il peut, 
entre autres choses, décider à quel programme en cours d'exécution il va 
rendre la main.

Le fait que l'ordonnanceur interrompe un processus et sauve son état s'ap-
pelle une commutation de contexte. Afin de pouvoir choisir parmi tous les
processus lequel exécuter lors de la prochaine interruption, le système d'ex-
ploitation conserve pour chaque processus une structure de données nommée
PCB (pour l'anglais Process Control Bloc ou bloc de contrôle du processus).
Le PCB est simplement une zone mémoire dans laquelle sont stockées di-
verses informations sur le processus.

Pour choisir parmi les processus celui auquel il va donner la main, l'or-
donnanceur conserve les PCB dans une structure de donnée, par exemple
une file.

## 2. Commande UNIX de gestion des processus

Dans les systèmes UNIX, la commande ```ps``` (pour l'anglais process status ou état des processus) permet d'obtenir des informations sur les processus en cours d'exécution.

```
$ ps -a -u -x
```

La colonne STAT indique l'état du processus (la première lettre en majuscule). 

- R : *running* ou *runnable*, le processus est dans l'état prêt ou en exécution
(la commande ps ne différencie pas ces deux états) ;

- S : *sleeping*, le processus est en attente.

Les colonnes START et TIME indiquent respectivement l'heure ou la date à la-
quelle le programme a été lancé et le temps cumulé d'exécution du processus
correspondant (c'est-à-dire le temps total pendant lequel le processus était
dans l'état « en exécution»). Enfin, la colonne COMMAND indique la ligne de
commande utilisée pour lancer le programme (elle est tronquée dans notre
exemple pour des raisons de place).

## 3. Programmation Concurente en Python

Afin d'illustrer les problématiques d'interblocage dans un cadre plus
contrôlé que dans un système d'exploitation, nous donnons ici une introduction 
à la programmation multithread en Python. Comme nous l'avons
déjà expliqué, un thread est un « sous-processus» démarré par un processus et 
s'exécutant de manière concurrente avec le reste du programme. Le
module threading de la bibliothèque standard Python permet de démarrer
des threads.

Recopier et exécuter ce code.

```python
import threading

def hello (n) :
  for i in range(5):
    print ("Je suis le thread", n, "et ma valeur est" : i)
  print ("______ Fin du Thread ", n)

for n in range(4):
  t = threading.Thread(target=hello, args=[n])
  t.start()
```

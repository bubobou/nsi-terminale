r1 = creerRationnel(1, 2)
afficher(r1)                    # pour vérifier
den = denominateur(r1)
print(den)                      # pour vérifier
r2 = creerRationnel(1, 3*den)
afficher(r2)                    # pour vérifier
r = ajouter(r1, r2)
afficher(r)                     # pour vérifier
egal(r, creerRationnel(2, 3))

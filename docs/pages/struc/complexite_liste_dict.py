import time
import matplotlib.pyplot as plt

def fabrique_liste(nb):
    lst = [k**2 for k in range(nb)]
    return lst

def fabrique_dict(nb):
    dct = {}
    for k in [k**2 for k in range(nb)]:
        dct[k] = 42
    return dct


def cout(elt,structure):
    t=time.time_ns()
    r = elt in structure
    t = time.time_ns() - t
    return t
    
# lst = fabrique_liste(1000)
dct = fabrique_dict(10000)



N = [2**n for n in range(10,20)]

Tdict = [cout('a',fabrique_dict(n)) for n in N]
Tlist = [cout('a',fabrique_liste(n)) for n in N]

plt.plot(N,Tdict)
plt.plot(N,Tlist)
plt.show()
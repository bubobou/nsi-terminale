---
hide:
  - footer
---

| Contenus     | Capacités attendues |  
|:-------------|:--------------------|
|Listes, piles, files : structures linéaires.| Distinguer des structures par le jeu des méthodes qui les caractérisent.|
||Choisir une structure de données adaptée à la situation à modéliser.|
|Dictionnaires, index et clé. | Distinguer la recherche d’une valeur dans une liste et dans un dictionnaire.|

# Structures de données linéaires


## 1. Structure de listes chaînées

!!! note "Définition"

    Une liste chaînée permet de représenter une liste d'élément dans une séquence finie.
    Deux éléments succesifs y sont liés entre eux, permettant ainsi le parcours de l'ensemble
    de la liste.

    Pour chaque élément de la liste on enregistre en mémoire sa valeur, mais aussi une référence à 
    l'élément suivant sous la forme de l'adresse mémoire de celui-ci.

!!! example "Un premier exemple de liste chaînée"

    Ci-dessous est illustrée une liste de 4 éléments, dont les valeurs sont respectivement
     1, 2, 3, et 4.

     ![l](../../images/liste1.svg)

     Chaque élément de la liste est matérialisé par un emplacement en mémoire contenant d'une part sa valeur 
     (dans la case de gauche) et d'autre part l'adresse mémoire de la valeur suivante (dans la case de droite).
     Pour ce qui est du dernier élément, qui ne possède pas de valeur suivante, on utilise une valeur spéciale 
     désignée ici par le symbole $\bot$ et marquant la fin de la liste.







## 1.1 Interface d'une cellule

Pour représenter une liste chaînée nous pouvons utiliser une classe qui décrit une cellule de 
cette liste. Les objets instanciés avec cette classe comporteront ainsi un attribut `valeur`
 enregistrant la valeur de cette cellule, ainsi qu'un deuxième attribut : `suivante` pour la
 cellule suivante de la liste.

!!! example "Implémentation de la classe `Cellule`"

      ```python
        class Cellule : 
          """Une cellule d'une liste chaînée"""
          def __init__(self,v,s):
            self.valeur = v
            self.suivante = s
      ```

!!! question "Comment signifier la fin d'une liste?"

    Lorsqu'il n'y a pas de cellule suivante, c'est-à-dire lorsque l'on considère la dernière cellule 
    de la liste, on donne à l'attribut suivante la valeur `None`. Dit autrement, `None` est notre 
    représentation du symbole $\bot$

!!! info "Construction d'une liste"

    Pour construire une liste, il suffit d'appliquer le constructeur de la classe Cellule autant de fois 
    qu'il y a d'éléments dans la liste. Ainsi, l'instruction ci-dessous permet de construire la liste donnée
    en exemple plus haut :

    ```python
    liste = Cellule(l, Cellule(2, Cellule(3, Cellule(4, None))))
    ```
    
    La valeur contenue dans la variable `liste` est l'adresse mémoire de l'objet contenant la valeur `1`.

    On peut schématiser cette liste de la façon suivante :

    ![l2](../../images/liste2.svg)

!!! tips "Autre implémentation"

    Il est tout à fait possible d'implémenter une structure de liste autrement qu'avec une classe, par
    exemple en utilisant des tuples :

    ```python
    liste = (1,(2,(3,(4,None))))
    ```

!!! example "Exercice 1"

      Dans l'IDE ci-dessous :  

      - définir une classe Cellule,  
      - construire une liste de quelques éléments,  
      - écrire les instructions permettant d'accéder à ces éléments.  

      {{ IDEv()}}

  
## 1.2. Opération sur les listes

Vous vous êtes certainement rendu compte en réalisant l'exercice précédent que l'accès aux valeurs
d'une liste n'est pas des plus pratique tel quel. Nous allons donc programmer quelques opérations
permettant de manipuler ces listes.

### 1.2.1 Déterminer la longueur d'une liste

Calculer la longueur d'une liste chaînée, revient à déterminer le nombre de cellules qu'elle
contient. Il s'agit donc de parcourir la liste, de la première cellule jusqu'à la dernière cellule,
en suivant les liens qui relient les cellules entre elles.
 
On peut réaliser ce parcours, au choix, avec une fonction récursive ou avec une boucle.

Dans les deux cas, on écrit une fonction longueur qui reçoit une liste `lst` en argument 
et renvoie sa longueur.

```python
def longueur(lst) :
  """renvoie ta tongueur de ta tiste lst"""
  ...
```

!!! example "Avec une fonction récursive"

    Il faut distinguer le cas de base, c'est-à-dire une liste vide ne contenant aucune cellule,
    du cas général, c'est-à-dire une liste contenant au moins une cellule.

    Dans le premier cas il suffit de renvoyer 0, dans l'autre cas, il faut renvoyer 1, pour la 
    première cellule, plus la longueur du reste de la liste.

    Écrire dans l'IDE ci-dessous une telle fonction.

    {{ IDEv() }}

!!! example "Avec une boucle"

    On commence par se donner deux variables : une variable `n` contenant la longueur que l'on calcule 
    et une variable `c` contenant la cellule courante du parcours de la liste.

    Initialement, n vaut 0 et c prend la valeur de Ist, c'est-à-dire None si la liste est vide et 
    la première cellule sinon.

    Le parcours est ensuite réalisé avec une boucle while.

    Écrire dans l'IDE ci-dessous une telle fonction.

    {{ IDEv() }}

!!! note "Complexité du calcul de la longueur"

    Il est clair que la complexité du calcul de la longueur est directement proportionnelle à la longueur 
    elle-même, puisqu'on réalise un nombre constant d'opérations pour chaque cellule de la liste.
    
    Ainsi, pour une liste lst de mille cellules, longueur(lst) va effectuer mille tests, mille appels 
    récursifs et mille additions dans sa version récursive, et mille tests, mille additions et deux 
    mille affectations dans sa version itérative.


### 1.2.2 Rechercher le n-ième élément d'une liste

Écrivons maintenant une fonction qui permette de retrouver la valeur du n-ième élément d'une liste 
chainée.

```python
def nieme_element(n, lst):
  """renvoie le n-ième élément de la liste lst
  les éléments sont numérotés à partir de 0"""
```

!!! example "Avec une fonction récursive"

    Il faut tout d'abord s'assurer que la liste n'est pas vide, auquel nous levons une exception.

    ```python
    if lst is None:
      raise IndexError("indice invalide")
    ```
    Si en revanche la liste lst n'est pas vide, il y a deux cas de figure à considérer :
    
    - si `n = 0`, c'est que l'on demande le premier élément de la liste et il est alors renvoyé; 
    - sinon, il faut continuer la recherche dans le reste de la liste. Pour cela, on fait un appel 
     récursif à `nieme_element` en diminuant de un la valeur de `n`.

    Écrire dans l'IDE ci-dessous une telle fonction.

    {{ IDEv() }}


### 1.2.3 Concaténer deux listes

La concaténation de deux listes consiste à mettre bout à bout les éléments de deux listes données. 
Ainsi, si la première liste contient 1,2,3 et la seconde 4,5 alors le résultat de la concaténation 
est la liste 1,2,3,4,5.

Nous choisissons d'écrirela concaténation sous la forme d'une fonction `concatener` qui reçoit deux
listes en arguments et renvoie une troisième liste contenant la concaténation.

```python
def concatener(ll, 12):
  """concatène les listes 11 et 12,
  sous la forme àJune nouvelle liste"""
```

!!! example "Avec une fonction récursive"

    On peut distinguer deux cas :

    - si la première liste `l1` est vide, on se contente de renvoyer la liste `l2`;  
    - sinon le premier élément de la concaténation est le premier élément de `l1` et le reste de la 
     concaténation est obtenu récursivement en concaténant le reste de `l1` avec `l2`.

     Écrire dans l'IDE ci-dessous une telle fonction.

    {{ IDEv() }}

!!! warning "État des listes après concaténation"

    Il est important de comprendre ici que les listes passées en argument à la fonction `concatener` 
    ne sont pas modifiées. Plus précisément, les éléments de la liste `l1` sont copiés et ceux de `l2` 
    sont partagés avec la liste renvoyée par la fonction.

!!! note "Complexité de la concaténation"

    Il est clair que le coût de la fonction `concatener` est directement proportionnel à la 
    longueur de la liste `l1`, puisque celle-ci est tout d'abord parcourue dans son intégralité. En revanche,
    ce coût ne dépend pas de la longueur de la liste `l2`.


### 1.2.4 Renverser une liste

Nous allons maintenant écrire une fonction `renverser` qui, lorsqu'elle reçoit en argument une liste comme 
`1,2,3,` renvoie  la liste renversée `3,2,1`. 

!!! example "Avec une fonction récursive"

    La fonction récursive ci-dessous permet bien d'obtenir le résultat attendu, néanmoins elle est 
    particulièrement coûteuse.

    ```python
    def renverser(lst):
      if lst is None:
        return None
      el se :
        return concatener(renverser(lst.suivante),Cellule(lst.valeur, None))
    ```

    Pour renverser une liste de 1000 éléments, cette fonction exécute près d'un demi-million d'opérations !

    En effet, il faut commencer par renverser une liste de 999 éléments, puis concaténer le résultat
    avec une liste d'un élément. Comme on l'a vu, cette concaténation coûte 999 opérations.
    
    Et pour renverser la liste de 999 éléments, il faut renverser une liste de 998 éléments puis concaténer
    le résultat avec une liste d'un élément.
    
    Et ainsi de suite. On total, on a donc au moins 999 + 998 + · . . + 1 = 499 500 opérations.

## 1.3 Encapsulation

Jusqu'à présent nous avons utilisé la classe `Cellule` afin de créer des listes, nous allons
voir maintenant comment encapsuler une liste chaînée dans un objet, c'est-à-dire cacher la 
représentation de la liste derrière un objet et proposer des méthodes pour la manipuler.

Nous allons ainsi réinvestir la classe `Cellule` ainsi que les fonctions définies précédemment.

Pour cela nous allons définir une classe Liste comportant uniquement l'attribut `tete`. Celui-ci 
désigne la tête de la liste, lorsque celle-ci n'est pas vide (et `None` sinon). Le constructeur 
initialise l'attribut `tete` avec la valeur `None`.

```python
class Liste:
  """une liste chainée"""

  def __init__(self):
    self.tete = None
```

Une des premières méthodes que l'on implémente usuellement avec ce type de structure de données
doit permettre de savoir si la liste est vide.

```python
  def est_vide(self):
    return ...
```

La méthode suivante doit permettre d'ajouter un élément (un objet de la classe Cellule) en 
tête de la liste.

```python
  def ajoute(self, x):
    self.tete = ...
```

On peut maintenant réutiliser nos fonctions, à savoir `longueur`, `nieme_element`, `concatener` 
ou encore `renverser`, comme autant de méthodes de la classe `Liste`. 

Ainsi, on peut écrire par exemple :

```python
def taille(self):
  return longueur(self.tete)
```
qui ajoute à la classe `Liste` une méthode `taille`, qui nous permet d'écrire
`lst.taille()` pour obtenir la longueur de la liste `lst`.

!!! example "class Liste"

    Écrire dans l'IDE ci-dessous l'implémentation de la classe `Liste`.

    {{ IDE() }}



## 2. Principes des Piles et des Files

Les structures de pile et de file permettent toutes deux de stocker des
ensembles d'objets et fournissent des opérations permettant d'ajouter ou de
retirer des objets un à un. Les éléments retirés ne sont cependant, ni dans
une structure ni dans l'autre, retirés dans un ordre arbitraire, et chacune de
ces structures a sa règle.


!!! info "Principe de la Pile"

    ![container](../../images/container.jpeg "credit:David Dibert"){width=400px}

    Dans une pile (en anglais stack), chaque opération de retrait retire
    l'élément arrivé le plus récemment. On associe cette structure à l'image
    d'une pile d'assiettes, dans laquelle chaque nouvelle assiette est ajoutée
    au-dessus des précédentes, et où l'assiette retirée est systématiquement
    celle du sommet. C'est aussi le principe de l'empilement des containers sur
    les cargos.


    Cette situation est nommée *Dernier Entré/Premier Sorti* (*DEPS*), ou
    plus couramment en anglais *LIFO* (Last In, First Out).

    Exemples de données stockées sous forme de pile :

    - les appels d'une fonction récursive  
      ![pile d'appel de fibonacci](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/data/pile_fibo.webp "credit:Gilles Lassus")
    - la liste des pages parcourues dans un navigateur internet : la fonction «Back» 
    permet de «dépiler» peu à peu les pages précédemment parcourues.

!!! info "Principe de la File"

    Dans une structure de file en revanche (en anglais queue), chaque opération 
    de retrait retire l'élément qui avait été ajouté le premier. On associe cette 
    structure à l'image d'une file d'attente, dans laquelle des personnes arrivent 
    à tour de rôle, patientent, et sont servies dans leur ordre d'arrivée.

    ![file](../../images/Simpson-file-dattente.jpeg){width=200px}

    Cette discipline est nommée *Premier Entré, Premier Sorti* (*PEPS*), ou
    plus couramment en anglais *FIFO* (First In, First Out).


    Exemples de données stockées sous forme de file :

    - les documents envoyés à l'imprimante sont traitées dans une file d'impression.  
    - la «queue» à la cantine est (normalement) traitée sous forme de file. 


Classiquement, chacune de ces deux structures a une interface proposant au
minimum les quatre opérations suivantes :

- créer une structure initialement vide,
- tester si une structure est vide,
- ajouter un élément à une structure,
- retirer et obtenir un élément d'une structure.
  
On s'attend généralement à ce que les piles et les files soient des structures homogènes, 
c'est-à-dire que tous les éléments stockés aient le même type. 

Le plus souvent, les structures de pile et de file sont également considérées 
mutables : chaque opération d'ajout ou de retrait d'un élément modifie la pile ou 
la file à laquelle elle s'applique. 

## 3. Structure de Pile


![illustration_pile](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/data/gifpile.webp)

### 3.1 Interface d'une pile

Comme expliqué précédemment, une pile travaille en mode LIFO (Last In First Out). Pour être utilisée, l'interface d'une pile doit permettre a minima :

- de savoir si une pile est vide;
- la création d'une pile vide;
- l'ajout d'un élément dans la pile (qui sera forcément au dessus). On dira qu'on **empile**;
- le retrait d'un élément de la pile (qui sera forcément celui du dessus) et le renvoi de sa valeur. On dira qu'on **dépile**.

Pour présenter une interface de pile, on utilisera la notation `Pile [T]` le type des
piles contenant des éléments de type `T`. 

!!! example "Savoir si une pile est vide"

    La méthode `est_vide` prend en paramètre une pile et renvoie un booléen indiquant la vacuité 
    de la pile.

    ```python
    est_vide(Pile[T]) -> bool
    ```

!!! example "Création d'une pile vide"

    La fonction `creer _pile` de création d'une pile vide ne prend aucun paramètre et renvoie 
    une nouvelle pile.

    ```python
    creer_pile() -> Pile[T]
    ```

    Si l'on définie une classe `Pile` cette fonctionnalité sera assurée par le constructeur.

!!! example "Empiler un élément"

    La fonction `empiler` prend en paramètres une pile et l'élément à ajouter, d'un type `T`
    correspondant à celui des éléments de la pile. Cette opération ne produit pas de résultat.

    ```python
    empiler(Pile[T], T) -> None
    ```

!!! example "Dépiler un élément"

    La fonction `depiler` prend en paramètre la pile et renvoie l'élément qui en a été retiré.

    ```python
    depiler(Pile[T]) -> T
    ``` 

    Cette  opération suppose que la pile est non vide et elle devra donc lever une exception 
    dans le cas contraire. 


### 3.2 Utilisation d'une interface de pile

!!! example "Exercice"

    === "Énoncé"

        On considère une classe ```Pile()``` et l'enchaînement d'opérations ci-dessous.
        Écrire à chaque étape l'état de la pile ```p``` et la valeur éventuellement renvoyée.

        On prendra pour convention que la tête de la pile est à droite.

        ```python
        1. p = Pile() 
        2. p.empiler(3)   
        3. p.empiler(5)  
        4. p.est_vide()  
        4. p.empiler(1)  
        5. p.depiler()  
        6. p.depiler() 
        7. p.empiler(9)  
        8. p.depiler()  
        9. p.depiler() 
        10. p.est_vide() 
        ```

    === Correction

        ... bientôt

## 3.3 Implémentation(s) d'une pile

L'objectif est de créer une classe ```Pile```. Chaque objet ```Pile``` disposera des méthodes suivantes :

- `__init()__`: créer une pile vide.
- ```est_vide()``` : indique si la pile est vide.
- ```empiler()``` : insère un élément en haut de la pile.
- ```depiler()``` : renvoie la valeur de l'élément en haut de la pile ET le supprime de la pile.
- ```__str__()``` : permet d'afficher la pile sous forme agréable (par ex : ```|3|6|2|5|```) par ```print()```

#### 3.3.1 À l'aide du type ```list``` de Python 

!!! example "Implémentation"

    Implémenter la classe Pile décrite au-dessus en utisant le type `list` python comme
    structure de pile.

    {{IDE()}}

    Tester l'implémentation avec les instructions suivantes :

    ```python
    >>> p = Pile()
    >>> p.empile(5)
    >>> p.empile(3)
    >>> p.empile(7)
    >>> print(p)
    ```

#### 3.3.2 À l'aide d'une liste chaînée et de la classe  ```Cellule``` créée au 1.1

Rappel de la définition de la classe ```Cellule``` telle que nous l'avons créée 
au début de ce cours :

```python linenums='1'
class Cellule :
    def __init__(self, contenu, suivante):
        self.contenu = contenu
        self.suivante = suivante
```


!!! example "Exercice "
    
    À l'aide cette classe, re-créer une classe ```Pile``` disposant exactement de la 
    même interface que dans l'exercice précédent.


    {{IDE()}}

    Tester l'implémentation avec les instructions suivantes :

    ```python
    >>> p = Pile()
    >>> p.empile(5)
    >>> p.empile(3)
    >>> p.empile(7)
    >>> print(p)
    ```

### 3.4 Application des piles


!!! example "Exercice"
    
    Simulez une gestion de l'historique de navigation internet, en créant une classe
     ```Nav``` qui utilisera une pile.

    Attention, il ne faut pas réinventer la classe ```Pile```, mais s'en servir !
        
    Exemple d'utilisation :

    ```python 
    >>> n = Nav()
    >>> n.visite('lemonde.fr')
    page actuelle : lemonde.fr
    >>> n.visite('qwant.fr')
    page actuelle : qwant.fr
    >>> n.visite('atrium-sud.fr')
    page actuelle : atrium-sud.fr
    >>> n.back()
    page quittée : atrium-sud.fr
    >>> n.back()
    page quittée : qwant.fr
    ```

    {{ IDE ('pile')}}


## 4. Structure de File

![giffile](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/data/giffile.webp){: .center}

### 4.1 Interface d'une File

Comme expliqué précédemment, une file travaille en mode FIFO (First In First Out).
Pour être utilisée, une interface de file doit proposer a minima :

- de savoir si une file est vide
- la création d'une file vide
- l'ajout d'un élément dans la file. On dira qu'on **enfile** (en anglais *enqueue*).
- le retrait d'un élément de la file et le renvoi de sa valeur. On dira qu'on **défile**
(en anglais *dequeue*) .


La représentation la plus courante d'une file se fait horizontalement, en enfilant par 
la gauche et en défilant par la droite :

Pour exprimer l'interface des files, nous notons `File[T]` le type des files contenant des 
éléments de type `T`. Ceci étant fixé, l'interface des file, est similaire à celle des piles. 

### 4.2 Exemple d'utilisation d'une File

!!! example "Exercice"

    === "Énoncé"

        On considère une classe ```File()``` et l'enchaînement d'opérations ci-dessous.
        Écrire à chaque étape l'état de la file ```f``` et la valeur éventuellement renvoyée.

        Par convention, on enfilera **à gauche** et on défilera **à droite**.

        ```python
        1. f = File()
        2. f.enfiler(3) 
        3. f.enfiler(1)
        4. f.est_vide()
        5. f.enfiler(4) 
        6. f.defiler() 
        7. f.defiler()
        8. f.enfiler(9) 
        9. f.defiler() 
        10. f.defiler()
        11. f.est_vide() 
        ```

    === "Correction"

        ... bientôt


## 4.3 Implémentation(s) d'une file

L'objectif est de créer une classe ```File```, disposant des méthodes suivantes :

- ```File()``` : crée une file vide.
- ```est_vide()```: indique si la file est vide.
- ```enfiler()``` : insère un élément en queue de file.
- ```defiler()``` : renvoie la valeur de l'élément en tête de la file ET le supprime de la file.
- ```__str__()``` : permet d'afficher la file sous forme agréable (par ex : ```|3|6|2|5|```) par ```print()```


#### 4.3.1 À l'aide du type ```list``` de Python 

!!! example "Implémentation"

    Compléter la classe File ci-dessous qui propose d'utiliser le type `list` python comme
    structure de file. On pourra notamment exploiter la méthode `insert`.

    {{IDE('debutfile')}}

    Tester l'implémentation avec les instructions suivantes :

    ```python
    >>> p = File()
    >>> p.enfiler(5)
    >>> p.enfiler(3)
    >>> p.enfiler(7)
    >>> print(p)
    >>> p.defiler()
    >>> print(p)
    ```

!!! warning "Coût de l'opération 'ajouter'"

    Dans cette implémentation on utilise un tableau pour lequel on ajoute une cellule
    puis on décale toutes les valeurs et ce pour chaque ajout. L'opération `ajouter`
    ne s'effectue donc pas en temps constant.


#### 4.3.2 À l'aide de deux piles

Il est bien sûr possible d'implémenter d'une file avec une liste chaînée comme pour la pile,
comme dans la partie 3.3.2. Néanmoins, du fait que la file comporte une tête et une queue, 
l'implémentation s'en trouve complexifiée. Il peut être plus simple d'aborder le problème en 
considérant deux piles.

Cette situation correspond à celle d'un jeu de cartes où l'on disposerait d'une pile constituant 
la pioche, au sommet de laquelle on prend des cartes, et d'une autre pile constituant la défausse.
On repose les cartes au sommet de cette dernière.

Chacun de ces deux paquets de cartes est bien une  pile, et ces deux paquets forment ensemble la
 réserve de cartes.

!!! tips "Comment créer une file avec 2 piles ?"

    L'idée est la suivante : on crée une pile d'entrée et une pile de sortie. 

    - quand on veut enfiler, on empile sur la pile d'entrée.
    - quand on veut défiler, on dépile sur la pile de sortie.
    - si celle-ci est vide, on dépile entièrement la pile d'entrée dans la pile de sortie.

    <center>
    <gif-player src="https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/data/2piles1file.gif" speed="1" play></gif-player>
    </center>


!!! example "Implémentation"

    Implémenter ci-dessous la classe File en utilisant deux piles. La classe Pile proposée
    utilise une structure de liste chaînée mais cela n'a aucune influence sur le code de 
    la classe File.

    {{IDE('file2pile')}}

## 5. Retour sur les dictionnaires


### 5.1 Notion de tableau associatif

Un **tableau associatif** est un type abstrait de données (au même titre que les listes, piles, files, vues précédemment). Ce type abstrait de données a la particularité de ne pas être totalement linéaire (ou «plat») puisqu'il associe des **valeurs** à des **clés**.  

Il est habituellement muni des opérations suivantes :

- ajout d'une nouvelle valeur associée à une nouvelle clé (on parlera de nouveau couple clé-valeur)
- modification d'une valeur associée à une clé existante
- suppression d'un couple clé-valeur
- récupération de la valeur associée à une clé donnée.

Un répertoire téléphonique est un exemple de tableau associatif :

- les clés sont les noms
- les valeurs sont les numéros de téléphone

En Python, le **dictionnaire** est une structure native de tableau associatif.

### 5.2 Dictionnaire et temps d'accès aux données 

!!! aide "TP : protocole de test pour comparer les temps d'accès aux données."

    **Indication :** on utilisera la fonction ```time.time()``` (après avoir importé le module
    ```time```) qui donne le nombre de secondes (à $10^{-7}$ près) écoulées depuis le 01 janvier 
    1970 à 00h00 (appelée [Heure Unix](https://fr.wikipedia.org/wiki/Heure_Unix) ou [Temps Posix](https://fr.wikipedia.org/wiki/Heure_Unix)).
    

    ```python
    >>> import time
    >>> time.time()
    1639001177.0923798
    ```

    La fonction ```time_ns()``` permet d'obtenir un nombre de nanosecondes, unité qui correspond
    mieux à l'ordre des grandeurs des valeurs mesurées.



#### Préparation des mesures

Considérons deux fonctions ```fabrique_liste()``` et ```fabrique_dict()``` capables de fabriquer respectivement des listes et des dictionnaires de taille donnée en paramètre.


```python
def fabrique_liste(nb):
    lst = [k**2 for k in range(nb)]
    return lst

def fabrique_dict(nb):
    dct = {}
    for k in fabrique_liste(nb):
        dct[k] = 42
    return dct
```


```python
>>> lst = fabrique_liste(10)
>>> dct = fabrique_dict(10)
>>> lst
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
>>> dct
{0: 42, 1: 42, 4: 42, 9: 42, 16: 42, 25: 42, 36: 42, 49: 42, 64: 42, 81: 42}
```

Le contenu de ces listes ou dictionnaires n'a pas grand intérêt. Dans nos mesures, on y cherchera une valeur qui n'y figure pas : la chaîne de caractères ```"a"```. On dit qu'on se place dans **le pire des cas**.

#### Mesures des temps de recherche


!!! example "Temps de recherche pour les listes"

    Les instructions suivantes permettent de comparer le temps d'exécution d'une recherche 
    dans une liste, pour différentes tailles de celle-ci.

    - avec 10 valeurs :

    ```python
    lst = fabrique_liste(10)
    t = time.time_ns()
    "a" in lst
    t = time.time_ns() - t
    print(t)
    ```

    - avec 100 valeurs :

    ```python
    lst = fabrique_liste(100)
    t = time.time_ns()
    "a" in lst
    t = time.time_ns() - t
    print(t)
    ```

    - avec 1000 valeurs :

    ```python
    lst = fabrique_liste(1000)
    t = time.time_ns()
    "a" in lst
    t = time.time_ns() - t
    print(t)
    ```

    Quelles sont les conclusions que vous pouvez tirer de vos mesures ?


!!! example "Temps de recherche pour les dictionnaires"

    On va rechercher si ```"a"``` est une clé valide pour notre dictionnaire.
    On reproduit le même type d'instructions que celle écrites ci-dessus pour les listes.

    - avec 10 valeurs :

    ```python
    dct = fabrique_dict(10)
    t = time.time_ns()
    "a" in dct
    t = time.time_ns() - t
    print(t)
    ```

    De même que pour les listes on peut adapter ces instructions afin de mesurer le temps de recherche pour 100 et 1000 valeurs.

    Quelles sont les conclusions que vous pouvez tirer de vos mesures ?


!!! note "Temps de recherche :heart:"

    Il y a une différence fondamentale à connaître entre les **temps de recherche** 
    d'un éléments à l'intérieur :

    - d'une **liste** : temps **proportionnel** à la taille de la liste.
    - d'un **dictionnaire** : temps **constant**, indépendant de la taille de la liste.

!!! warning "Temps d'accès"

    Attention : en ce qui concerne **les temps d'accès** à un élément, la structure de
    tableau dynamique des listes de Python fait que ce temps d'accès est aussi en temps 
    constant (comme pour les dictionnaires). On voit alors que les listes Python ne sont
    pas des *listes chaînées*, où le temps d'accès à un élément est directement proportionnel
    à la position de cet élément dans la liste.

---

références :

 - T.Balabonski, S.Conchon, JC.Filliâtre, K.Nguyen  *Numérique et Sciences Informatiques*, Ed. Ellipses
 - G.Lassus (lycée François Mauriac, Bordeaux), [Terminale NSI - Liste, Piles, Files](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/cours/)
class Cellule:
    def __init__(self,contenu,suivante):
        self.contenu = contenu
        self.suivante = suivante

class Pile:
    def __init__(self):
        self.data = None
    def est_vide(self):
        return self.data is None
    def empiler(self, valeur):
        self.data = Cellule(valeur,self.data)
    def depiler(self):
        if self.est_vide() :
            raise IndexError("la pile est vide")
            return None
        else :
            valeur = self.data.contenu
            self.data = self.data.suivante
            return valeur
    def __str__(self):
        ch = "|"
        c = self.data
        while c is not None:
            ch = "|"+str(c.contenu)+ch
            c = c.suivante
        return ch


class File:
    def __init__(self) -> None:
        self.entree = Pile()
        self.sortie = Pile()

    def file_est_vide(self):
        return self.entree is None and self.sortie is None
    
    def enfiler(self, valeur):
        self.entree.empiler(valeur)

    def defiler(self):
        if self.file_est_vide() :
            raise IndexError("la file est vide")
            return None
        elif self.sortie.est_vide():
            while not self.entree.est_vide() :
                self.sortie.empiler(self.entree.depiler())
        else :
            self.sortie.depiler()

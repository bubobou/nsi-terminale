---
hide:
  - footer
---

# Arbres

??? quote "Éléments du référentiel"

    |Contenus       |     Capacités attendues |
    |:-------------|:-------------|
    |Arbres : structures hiérarchiques.| Identifier des situations nécessitant une structure de données arborescente.|
    |Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits. | Évaluer quelques mesures des arbres binaires (taille, encadrement de la hauteur, etc.)|
    |Algorithmes sur les arbres binaires | Calculer la taille et la hauteur d’un arbre. |
    |Algorithmes sur les arbres binaires de recherche |Parcourir un arbre de différentes façons (ordres infixe, préfixe ou suffixe ; ordre en largeur d’abord).|
    |Algorithmes sur les arbres binaires de recherche |Rechercher une clé dans un arbre de recherche, insérer une clé | 

## 1. Structures arborescentes

Dans un chapitre précédent nous avons étudié les structures de données linéaires qui s'avéraient efficaces dans de nombreux cas pour manipuler l'information. Néanmoins, en n'utilisant qu'une seule dimension on se retrouve limité, notamment dès lors que l'on souhaite hiérachiser les données. Les structures arborescente sont particulièrement adaptée au traitement des données hiérarchisées.

Dans notre vie quotidienne nous retrouvons un peu partout des structures en arbres, et nous allons voir que leur étude permet de résoudre des problèmes informatiques intéressants.

### 1.1 Exemples de structure arborescentes

Un arbre généalogique est, par exemple, un graphe particulier où les sommets sont les personnes et les arêtes sont les liens de parenté directe entre ces personnes.

![arbre_genealogique](../../images/arbre_gene.png){width=30%}

Un autre exemple, plus proche de l’informatique, concerne l’arborescence de fichiers stockés
sur un ordinateur. Par exemple, dans la figure ci-dessous, le répertoire principal de la machine
contient une image `souris.jpg` ainsi que trois répertoires. Chacun de ses répertoires 
contient à son tour un certain nombre de fichiers et/ou d’autres répertoires. Un répertoire peut
aussi être vide, comme le répertoire `System`. Notez que la représentation de l’arbre est inversé,
le tronc tout en haut et les branches partant vers le bas... C’est souvent la tradition en informatique.

![arbre_sys](../../images/arbre_sys.png){width=60%}

Mais vous avez aussi vu des arbres dans votre cursus scolaire. Par exemple, si on vous
décrit une situation où une urne contient deux boules rouges et trois boules bleues, et qu’on
vous demande de calculer la probabilité de tirer deux boules de couleurs différentes, si on ne
fait pas de remise, alors vous savez qu’une méthode de résolution consiste à utiliser un arbre
de probabilités, comme illustré ci-dessous. La probabilité d’une branche est alors le produit des
probabilités le long de la branche.

![arbre_proba](../../images/arbre_proba.png){width=60%}


### 1.2 Définitions

Un arbre est un type de graphe. Qu'est ce qu'un graphe ? Nous les rencontrerons dans un prochain chapitre, mais nous pouvons déjà les définir comment un ensemble de données (noeud) liées entre elles par des arrêtes.

Qu’ont en commun les arbres qu’on a vus jusque-là ? Ce sont des graphes particuliers en ce sens qu'ils sont tous d’un seul tenant et qu'ils ne sont pas orientées (il n'y a pas de sens de 
parcours privilégié). Si l'on utilise le vocabulaire lié aux graphes, on définit un arbre comme 
un graphe non orienté connexe et sans cycle. 

!!! note "Structure d'un arbre :heart:"

    Nous pouvons définir un arbre comme étant une structure hiérarchique de données, composée de **nœuds** :

    - Chaque nœud a exactement un seul nœud **père**, à l'exception du nœud **racine** qui 
     est le seul nœud à ne pas avoir de père. (oui, la racine d'une arbre est en haut).  
    - Chaque nœud peut avoir un nombre quelconque de **fils**, dont il est le père.  
    - Les nœuds qui n'ont pas de **fils** sont appelés des **feuilles** .  
    - Pour conserver l’analogie botanique, on dit qu’un chemin allant de la racine à une feuille
     est une **branche** de l’arbre.
    - Le nom de chaque nœud est appelé son **étiquette**.  

    ![](../../images/arbre_def.png){width=50%}


!!! note "Description d'un arbre :heart:" 

    La **taille** d'un arbre est son nombre total de nœuds. Dans l'arbre représenté ci-dessus, elle vaut 8.

    L'**arité** d'un nœud est son nombre de fils. Ici, l'arité de B vaut 2, celle de F vaut 1, celle de Z vaut 0.

    la **profondeur** d'un nœud est le nombre de nœuds de son chemin le plus court vers la racine.
    Ici, la profondeur de G est 3 (G-K-C), la profondeur de B est 2 (B-C), la profondeur de Z est 
    4 (Z-F-B-C), la profondeur de C est 1.

    La **hauteur** d'un arbre est la profondeur de son nœud le plus profond. Ici, la hauteur de 
    l'arbre est 4.

    Nous prendrons comme convention que :  

    - si un arbre est réduit à un seul nœud-racine, sa hauteur sera 1.  
    - si un arbre est vide, sa hauteur est 0.  


## 2. Arbres binaires

Parmi tous les arbres possibles, on distingue une classe importante en pratique : les arbres
binaires. Ce sont des arbres enracinés dont chaque sommet a au plus 2 fils : on distingue
alors d’ailleurs son fils gauche de son enfant droit. Les arbres ci-dessous sont donc des
arbres binaires :

![arbres_binaires](../../images/arbre_bin.png){width=80%}

### 2.1 Définition récursive

On peut donner une définition  de ces arbres, plus proche de leur représentation 
dans un ordinateur. Il s’agit d’une définition récursive : cela veut dire qu’on s’autorise
à utiliser la notion d’arbre binaire pour définir ce qu’est un arbre binaire.

Un arbre binaire est :

- soit l’arbre vide, qu’on note souvent `nil` (et qu’on ne représente généralement pas dans
les dessins);  
- soit une racine ayant un enfant gauche et/ou un enfant droit, qui sont aussi des arbres
binaires.

### 2.2 Sous-arbres d'un arbre binaire

Chaque nœud d'un arbre binaire ne pouvant pas avoir plus de 2 fils, il est possible de séparer le «dessous» de chaque nœud en deux **sous-arbres** (éventuellement vides) : le **sous-arbre gauche** et le **sous-arbre droit**.

!!! example "Exemple de sous-arbres"

    ![sous-arbre](../../images/sousarbres.png)

    - Les deux sous-arbres représentés ici sont les sous-arbres du nœud-racine `T`.  
    - Le nœud `O` admet comme sous-arbre gauche le nœud `H` et comme sous-arbre droit le nœud `N`.
    - Les feuilles `P`, `H` et `N` ont pour sous-arbre gauche et pour sous-arbre droit l'arbre vide.


### 2.3 Taille d'un arbre binaire

#### Arbre complet

Un arbre binaire est dit complet lorsqu'aucun de ces fils gauche ou droit n'est manquant :

![arbre-complet](../../images/arbre_complet.png){width=30%}

Le premier étage comporte $1$ nœud, le second $2$ nœuds, le troisième $4$ nœuds ...

Soit un arbre binaire complet de hauteur $h$, on peut calculer sa taille comme la somme $S$
des $h$ premiers termes d'une suite géométrique de raison $2$ et de premier terme $1$, d'où :

$$S = \dfrac{1-2^h}{1-2} = 2^h - 1$$

On en déduit alors l'encadrement de la taille $t$ d'un arbre binaire de hauteur $h$ :

$$h \leq t \leq 2^h - 1$$


## 3. Parcours

Les arbres étant une structure hiérarchique, leur utilisation implique la nécessité d'un parcours des valeurs stockées. Par exemple pour toutes les récupérer dans un certain ordre, ou bien pour en chercher une en particulier.

Il existe plusieurs manières de parcourir un arbre.

### 3.1 Parcours préfixe


    
!!! note inline end ""

    ![arbo-fichier](../../images/arbre_fichiers.png)
    
!!! note ""
    
    Considérons désormais des problèmes de la vie réelle que l’on va pouvoir traiter à l’aide
    d’arbres. Reprenons l’exemple de l’arborescence de fichiers de la figure vue précédemment.
     Parfois, on a  besoin d’aﬀicher cette arborescence sous forme d’une liste de fichiers.
     
     Un outil permet de faire cela sous un système d’opérations Unix, tel que Linux ou Mac : 
     l’utilitaire `ls` qui lorsqu’on l’exécute dans un terminal avec l’option `-R` pour signifier 
     qu’on souhaite rentrer récursivement dans les sous-répertoires, on obtient le résultat
    ci-contre.


Le programme `ls` commence par imprimer les noms des fichiers et répertoires contenus directement dans le répertoire racine. Ensuite, il imprime le contenu du premier répertoire
entièrement, avant de passer au second répertoire, puis le troisième répertoire. L’impression
du premier répertoire `/Users` est longue puisque, de même, il faut commencer par imprimer
le nom des deux répertoires, puis le contenu de chacun, et ainsi de suite.
Remis dans le contexte d’un arbre binaire (plutôt qu’un arbre quelconque comme pour
l’arborescence de fichiers), cela revient à considérer le parcours suivant :

![prefixe](../../images/arbre_prefixe.png){width=80%}

De plus, on imprime le contenu du sommet la première fois qu’on le visite, ce qui fait que,
dans ce cas, on imprimerait donc les sommets dans l’ordre : `1 | 2 | 4 | 5 | 3 | 6 | 7`

Un tel parcours, qu’on appelle parcours préfixe puisqu’il traite chaque nœud avant de
traiter ses enfants, peut être implémenté par un algorithme récursif :

```
fonction parcours_préfixe (nœud) :
    Si nœud ≠ nil alors
        afficher ( valeur [nœud ])
        parcours_préfixe ( enfant_gauche [nœud ])
        parcours_préfixe ( enfant_droit [nœud ])
```

!!! example "Exercice"

    Écrire dans l'IDE ci-dessous l'implémentation en Python du parcours préfixe.

    On pourra utiliser le contexte où un nœud est un objet ayant pour attribut
    "gauche" et "droit".

    {{ IDEv() }}

### 3.2 Parcours Postfixe

En matière d’aﬀichage d’un arbre binaire, l’évaluation d’une expression arithmétique
consiste en le parcours suivant :

![postfix](../../images/arbre_postfix.png){width=80%}

dans lequel on aﬀiche chaque nœud lors du dernier passage par celui-ci, après le traitement
de ses enfants gauche et droit. En opposition au parcours préfixe précédent, on appelle ce parcours le parcours postfixe.


On peut l’écrire de la manière suivante à l’aide d’un algorithme récursif :

```
fonction parcours_postfixe (nœud) :
    Si nœud ≠ nil alors
        parcours_postfixe ( enfant_gauche [nœud ])
        parcours_postfixe ( enfant_droit [nœud ])
        afficher ( valeur [nœud ])
```

### 3.3 Parcours Infixe

Nous avons vu deux parcours d’arbres différents : le parcours préfixe dans lequel on traite
d’abord le nœud avant de traiter son enfant gauche puis son enfant droit, et le parcours
postfixe où on commence par traiter les deux enfants avant de traiter le nœud lui-même.
On peut aisément imaginer un troisième parcours dans lequel on traite le nœud entre le
traitement de l’enfant gauche et celui de l’enfant droit : on appelle cela le **parcours infixe**.

Il peut se visualiser ainsi :

![infixe](../../images/arbre_infixe.png){width=80%}

et aﬀiche donc les nœuds dans l’ordre 4, 2, 5, 1, 6, 3 et 7. C’est comme si on avait aplatit
l’arbre en le passant sous une enclume.

L’algorithme récursif de ce parcours est naturellement le suivant :

```
fonction parcours_infixe (nœud) :
    Si nœud ≠ nil alors
        parcours_infixe ( enfant_gauche [nœud ])
        afficher ( valeur [nœud ])
        parcours_infixe ( enfant_droit [nœud ])        
```

Le parcours infixe est très naturel, en particulier à nouveau dans les calculatrices. 

Il permet d’aﬀicher à l’écran le calcul (stocké sous forme d’arbre par la calculatrice) sous un
format compréhensible par nous.

En fait, aﬀicher une expression :
    
- c’est aﬀicher le nombre $n$ si on est à une feuille ;  
- sinon, c’est aﬀicher une parenthèse ouvrante, aﬀicher l’enfant gauche, aﬀicher l’opération contenue dans le nœud courant, puis aﬀicher l’enfant droit et enfin aﬀicher une parenthèse fermante.  
   
!!! warning "Profondeur d'abord"

    Ces trois parcours ne diffèrent donc que par l’ordre dans lequel on visite la racine, l’enfant
    gauche et l’enfant droit.
    
    Ce sont tous des parcours en **profondeur d'abord**, ou DPS (Depth First Search). Ce qui signifie qu'un des deux sous-arbres sera totalement parcouru avant que l'exploration du deuxième ne commence. 

    
### 3.4 Parcours en largeur

Le parcours en **largeur d'abord** est un parcours étage par étage (de haut en bas) et de 
gauche à droite.

!!! example "exemple de parcours en largeur"

    ![bfs](../../images/BFS.png)

    L'ordre des lettres parcourues est donc T-Y-O-P-H-N.

## 4. Arbres binaires de recherche

!!! note "Définition :heart:"

    Un arbre binaire de recherche (ABR) est un arbre tel que chaque nœud x 
    de l’arbre vérifie les deux propriétés suivantes : 

    - toutes les valeurs des nœuds dans l’enfant gauche de x sont inférieures 
    à la valeur de x,
    - toutes les valeurs des nœuds dans l’enfant droit de x sont supérieures 
    à la valeur de x.


**Remarque** : le parcours infixe d'un ABR donne une suite croissante.

### 4.1 Recherche dans un ABR

Comment rechercher une valeur dans un ABR ? 

On peut utiliser un algorithme récursif qui descend dans l’arbre en partant de la
racine et en continuant à gauche ou à droite à l’aide d’une comparaison entre la valeur à chercher et le nœud courant. Notez la proximité entre cet algorithme et la recherche 
dichotomique qui décidait aussi de continuer dans la moitié gauche ou droite du tableau 
restant, à l’aide d’une comparaison entre la valeur cherchée et le milieu du tableau : 
c’est la racine de l’ABR qui joue le rôle du milieu du tableau.

![rech_abr](../../images/recherche_abr.png){width=70%}

!!! note "Algorithme de recherche dans un ABR :heart: "

    === "En pseudo-code"

        ```
        fonction est_présent_abr (nœud , x)
            si nœud = nil alors
                retourner Faux
            sinon si x = valeur [nœud] alors
                retourner Vrai
            sinon si x < valeur [nœud] alors
                retourner est_présent_abr ( enfant_gauche [nœud], x)
            sinon
                retourner est_présent_abr ( enfant_droit [nœud], x)
        ```

    === "En Python"

        ```python
        def contient_valeur(arbre, valeur):
            if arbre is None :
                return False
            if arbre.data == valeur :
                return True
            if valeur < arbre.data :
                return contient_valeur(arbre.left, valeur)
            else:
                return contient_valeur(arbre.right, valeur)
        ```

!!! question "Quelle est la complexité de cet algorithme ?  "

    À chaque appel récursif, on plonge dans l’enfant gauche ou l’enfant droit, donc on descend 
    d’un niveau à chaque appel. Ainsi, les appels récursifs suivent une branche (un chemin de 
    la racine à l’une des feuilles de l’arbre) de l’arbre et la complexité est donc proportionnelle 
    à la longueur maximale d’une de ses branches et donc à la heuteur de l'arbre.


!!! question "Comment la hauteur d’un ABR se compare-t-elle au nombre `n` de nœuds que l’arbre contient ?  "

    Malheureusement pas toujours très bien : la hauteur peut être de l’ordre de `n` dans le pire
    des cas. Mais si l’ABR est *équilibré*, c’est-à-dire qu’on 
    essaie de conserver autant de nœuds dans l’enfant gauche et l’enfant droit de n’importe 
    quel nœud de l’arbre, alors la hauteur devient proportionnelle à $log_2 n$. Dans ce cas, 
    la complexité de l’algorithme de recherche dans un ABR a complexité $O(log n)$, comme pour 
    la recherche dichotomique.

### 4.2 Ajouter dans un ABR

L'insertion d'une clé va se faire au niveau d'une feuille, donc au bas de l'arbre. 
Dans la version récursive de l'algorithme d'insertion, que nous allons implémenter, il 
n'est pourtant pas nécessaire de descendre manuellement dans l'arbre jusqu'au bon endroit : 
il suffit de distinguer dans lequel des deux sous-arbres gauche et droit doit se trouver la 
future clé, et d'appeler récursivement la fonction d'insertion dans le sous-arbre en 
question.

![ajout_abr](../../images/abr_ajout.png){width=70%}

!!! note "Ajout d'un noeud dans un ABR"

    === "en Python"

        ```python
        def insertion(arbre, valeur):
            if arbre is None :
                return Arbre(valeur)
            else :
                v = arbre.data
                if valeur <= v :
                    arbre.left = insertion(arbre.left, valeur)
                else:
                    arbre.right = insertion(arbre.right, valeur)
                return arbre
        ```

        *Remarque* : ici on laisse la possibilité d'insérer un noeud pour une valeur qui
        est déjà présente. On pourrait tout aussi bien l'empêcher.

---

Bibliographie⚓︎

  - Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.
  - [glassus.github.io](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.3_Arbres/cours/#54-insertion-dans-un-abr) G.LASSUS, Lycée François Mauriac - Bordeaux
  - Cours d'Introduction à l’informatique, Benjamin Monmege - Université Aix-Marseille

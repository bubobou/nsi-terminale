# IMPLEMENTATION AVEC UN COUPLE

def creerRationnel(n, d):
    """Entier x Entier --> Rationnel"""
    return (n, d)

def denominateur(r):
    """Rationnel --> Entier"""
    return r[1]
    
def ajouter(r1, r2):
    """Rationnel x Rationnel --> Rationnel"""
    # calculs du numérateur et du dénominateurs en procédant à une réduction au même dénominateur
    num = r1[0] * r2[1] + r2[0] * r1[1]
    den = r1[1] * r2[1]
    # simplification du rationnel en divisant le numérateur et le dénominateur par leur pgcd
    d = pgcd(num, den)
    return (num // d, den // d)

def egal(r1, r2):
    """Rationnel x Rationnel --> Booléen"""
    return r1 == r2

def afficher(r):
    """Rationnel --> str"""
    print(str(r[0]) + "/" + str(r[1]))

# Une fonction ne faisant pas partie de l'interface de la structure de données \
# mais utilisée dans la fonction ajouter
def pgcd(a, b):
    """Renvoie le pgcd des entiers positifs a et b"""
    return a if b == 0 else pgcd (b,a) if b > a else pgcd (a-b, b)
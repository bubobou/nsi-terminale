# IMPLEMENTATION AVEC UN DICTIONNAIRE ET PAS DE SIMPLIFICATIONS INTERMEDIAIRES

def creerRationnel(n, d):
    """Entier x Entier --> Rationnel"""
    return {"num": n, "den": d}

def denominateur(r):
    """Rationnel --> Entier"""
    return r["den"]
    
def ajouter(r1, r2):
    """Rationnel x Rationnel --> Rationnel"""
    # calculs du numérateur et du dénominateurs en procédant à une réduction au même dénominateur
    num = r1["num"] * r2["den"] + r2["num"] * r1["den"]
    den = r1["den"] * r2["den"]
    # pas de simplification !
    return {"num": num, "den": den}

def egal(r1, r2):
    """Rationnel x Rationnel --> Booléen"""
    return r1["num"] * r2["den"] == r2["num"] * r1["den"] # produit en croix

def afficher(r):
    """Rationnel --> str"""
    print(str(r["num"]) + "/" + str(r["den"]))
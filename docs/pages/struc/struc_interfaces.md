---
hide:
  - footer
---

# Structures de données, interfaces et implémentation

## 1. Types abstraits, structures de données ?

Les algorithmes opèrent sur des données (données en entrée, données auxiliaires), et sont en charge notamment, de manière centrale, de la **représentation des données** et de la **manipulation des données**.

On peut différencier classiquement deux niveaux de représentation :

* niveau ***Abstrait*** ou niveau ***Logique*** (on dit aussi ***Type Abstrait de Données*** ou ***TAD***)  
    * Ce niveau regroupe tout un ensemble de *routines* (procédures ou fonctions) abstraites, définissant certaines *opérations* de manipulation des données. 
    * L'ensemble de toutes ces routines est appelée l'*INTERFACE* (abstraite) de la structure de données abstraites. 
   
      **INTERFACE = Comment on s'en sert?**

  Utiliser un objet ou une fonctionnalité par son interface abstraite permet de raisonner indépendamment des détails de son implémentation, ce qui est une tâche qui revient à la *Structure de Données* :

* niveau ***Physique*** (Structure de données)
    * C'est la **Structure de données** qui est en charge de l'implémentation du type abstrait de données, en particulier :
        * de la Représentation concrète des données en mémoire
        * de l'implémentation des opérations de manipulation de données
    
     **IMPLÉMENTATION = Comment ils fonctionnent ?**


!!! example "INTERFACE vs IMPLÉMENTATION"

    On peut choisir comme image une machine à café à capsule, dans laquelle on peut distinguer :  


    - l' **INTERFACE** : les boutons, les voyants, le levier…  
    - l' **IMPLÉMENTATION** : les électrovannes, la pompe, le bloc de chauffe, voire le déroulement des opérations
  
    L'utilisateur n'a pas besoin de savoir comment fonctionne la machine à l'intérieur (*implémentation*), pour se faire un café (*interface*)...


    === "Interface"

        ![interface_cafe](../../images/nespressoInterface.webp){width=300px}

    === "Implémentation"

        ![implémentation_cafe](../../images/nespressoImplementation.png){width=300px}
    


!!! info "À RETENIR"

    * Une **Interface** ou une *Spécification* de la Structure de Données décrit:
        * **Quelles sont les valeurs possibles ?** et 
        * **Quelles sont les opérations avec lesquels on peut la manipuler?**, et 
        * **Quel est le résultat qu'ils produisent?**.
    * **À l'opposé**, Une **Implémentation** ou *Réalisation* de la Structure de Données :
        * est en charge de la **Représentation concrète des données en mémoire**
        * contient **le code et la manière précis** permettant de réaliser l'opération spécifiée par l'interface. 
      Il n'est pas nécessaire de connaître l'implémentation pour manipuler la structure de données.


## 2. Types Abstraits de Données (TAD) : niveau Interface / *Utilisateur*

En algorithmique, les algorithmes opèrent sur des données de natures différentes. 
Dans un premier niveau, du **point de vue Utilisateur**, il semble important et intéressant de proposer des algorithmes qui soient :

* indépendants d'un langage de programmation, et même 
* indépendants d'une implémentation particulière.

A ce premier niveau, les données sont manipulées de manière abstraite via une **interface** :

!!! info "Type Abstrait (de Données)"
    Un <bred>Type Abstrait (de Données) (TAD)</bred>, ou  <bred>Structure de Données Abstraite</bred> :fr:, ou <bred>Abstract Data Type (ADT)</bred> :gb:, est une collection/ensemble d'éléments/données définis par :

    * une **notation** pour les ***décrire***
    * des **Primitives** : des ***opérations*** agissant sur ses données/éléments (c'est-à-dire leur interface), indépendamment du langage de programmation.
    * une **Sémantique** : certaines ***propriétés*** que doivent vérifier ces opérations, pouvant se subdiviser en:
        * des **Pré-conditions** : Des conditions/propriétés qui ***définissent l'existence*** des opérations (lorsque celles-ci sont partiellement définies).
        * des **Axiomes** : pour décrire le ***comportement*** attendu des opérations.

Un type abstrait de données (TAD) correspond au point de vue de l'Utilisateur, et non pas de l'implémenteur. Il reviendra ensuite à la Structure de Données de les implémenter. 


!!! note "Principaux Types de Données Abstraits"
    * **Structures Linéaires / Séquentielles :**
        * Listes
        * Piles
        * Files
        * (Files de Priorité, etc..)
    * **Structures Récursives / Hiérarchiques / Arborescentes :**
        * (Listes)
        * Arbres (généraux)
        * Arbres Binaires, ...
    * **Structures Relationnelles :**
        * Graphes
    * **Structures à Accès par Clé :**
        * Tableaux (Statiques vs Dynamiques)
        * Dictionnaires, ou Tableaux Associatifs, ou Table d'Association


## 3. Structures de Données

### 3.1 Définition

!!! info "Structure de Données"
    Une <bred>Structure de Données</bred> est en charge d'**organiser**, de **stocker** et de **gérer** les données.
    Une Structure de données est une **implémentation** d'un type de abstrait de données, donc en particulier elle est en charge de :

    * la **Représentation concrète** des données en mémoire
    * l'**implémentation des opérations/primitives** du type abstrait de données


Une **structure de données** correspond au point de vue de l'implémenteur, et non pas de l'utilisateur (Type Abstrait de Données).
Un type de données abstrait peut donc être implémenté de plusieurs manières distinctes en des structures de données abstraites.

!!! example "en Python"
    En Python, on pourra penser par exemple aux structures de données (appelés types de données en Python) : `list` et `dict`.  

    :warning: En Python, la structure de données `list` est un faux-ami : il implémente le type abstrait de données **Tableau Dynamique**.


### 3.2 Opérations de base d'une Structure de Données : `CRUD`

Classiquement, une Structure de Données fournit normalement au minimum les opérations de base suivantes, usuellement notées <env>**CRUD**</env> (<b>C</b>reate, <b>R</b>ead, <b>U</b>pdate, <b>D</b>elete :gb:) :

* <b>C</b>reate / Insertion
* <b>R</b>ead / Lecture
* <b>U</b>pdate / Modification
* <b>D</b>elete / Suppression

## 4. Exemple : la structure *Rationnel*

### 4.1 Interface de la structure de données

On aimerait définir une **structure de données** appelée `Rationnel` correspondant à l'ensemble des nombres rationnels (noté $\mathbb Q$). Voici les opérations que l'on souhaite effectuer sur les rationnels :

- **Créer** un rationnel
- **Accéder** au numérateur et au dénominateur d'un rationnel
- **Ajouter**, **soustraire**, **multiplier**, **diviser** deux rationnels
- **Vérifier** si deux rationnels sont égaux ou non

On spécifie l'ensemble des opérations souhaitées en proposant l'**interface** suivante :

- `creerRationnel(n, d)` : crée un élément de type `Rationnel` à partir de deux entiers `n` (numérateur) et `d` (dénominateur). *Précondition* : $d\neq 0$. 
- `numerateur(r)` : accès au numérateur du rationnel `r` (renvoie un entier)
- `denominateur(r)` : accès au dénominateur du rationnel `r` (renvoie un entier non nul)
- `ajouter(r1, r2)` : renvoie un nouveau rationnel correspondant à la somme des rationnels `r1` et `r2`
- `soustraire(r1, r2)` : renvoie un nouveau rationnel correspondant à la différence des rationnels `r1` et `r2` (`r1` -`r2`)
- `multiplier(r1, r2)` : renvoie un nouveau rationnel correspondant au produit des rationnels `r1` et `r2`
- `egal(r1, r2)` : renvoie Vrai si les deux rationnels `r1` et `r2` sont égaux, Faux sinon.

On ajoute à cela une opération permettant d'afficher un rationnel sous la forme d'une chaîne de caractères :

- `afficher(r)` : affiche le rationnel `r` sous la forme d'une chaîne de caractères `'n/d'` où `n` et `d` sont respectivement le numérateur et le dénominateur de `r`.

### 4.2 Exemple d'utilisation de la structure

L'interface apporte toutes les informations nécessaires pour utiliser le type de données. 
Ainsi, le programmeur qui l'utilise n'a pas à se soucier de la façon dont les données sont 
représentées ni de la manière dont les opérations sont programmées. L'interface (uniquement) 
lui permet d'écrire toutes les instructions qu'il souhaite et obtenir des résultats corrects. 

Par exemple, il sait qu'il peut écrire le programme suivant pour manipuler le type `Rationnel` 
(écrit ici en Python mais on pourrait le faire dans un autre langage).


```Python
r1 = creerRationnel(1, 2)
den = denominateur(r1)  # den vaut donc 2
r2 = creerRationnel(1, 3*den)  # r2 représente le rationnel 1/6
r = ajouter(r1, r2)  # r est le résultat de 1/2 + 1/6
egal(r, creerRationnel(2, 3)) # doit renvoyer True puisque 1/2 + 1/6 = 2/3
```


### 4.3 Implémentations

Pour implémenter une structure de données, c'est-à-dire programmer les opérations sur la structure, il est nécessaire d'utiliser les structures de données déjà définies dans le langage utilisé. En Python, on peut avantageusement utiliser les types `int`, `float`, `boolean` ainsi que les tuples (avec le type `tuple`), les tableaux (avec le type `list`), les dictionnaires (avec le type `dict`), les ensembles (avec le type `set`).

Nous allons proposer deux implémentations : l'une avec un tuple (on aurait pu utiliser un tableau de manière identique) et l'autre avec un dictionnaire.

#### Une implémentation possible

On peut par exemple implémenter (= *programmer* concrètement) le type abstrait `Rationnel` en utilisant des couples (le type `tuple` de Python). Voici ce que pourrait alors être l'implémentation de certaines des opérations du type `Rationnel`.

>**Remarque** : on n'a écrit ci-dessous que les opérations permettant d'illustrer le propos.

 {{ IDE ('implementation')}}

On peut alors vérifier, même si on le savait déjà grâce à l'interface, que les instructions précédentes donnent des résultats corrects (on a pris le soin d'afficher certains résultats pour illustrer).

{{ IDEv ('verifInterface')}}


#### Une autre implémentation possible

Imaginons que le programmeur qui a implémenté le type abstrait `Rationnel` ait fait des choix différents :  

- il a utilisé des dictionnaires pour représenter les rationnels
- il a choisi de ne pas simplifier les fractions au fur et à mesure des calculs, ce qui implique une autre écriture du test d'égalité


{{ IDE ('implementation2')}}


On peut vérifier que l'on peut écrire exactement les mêmes instructions que précédemment et obtenir exactement les mêmes résultats, alors même que l'implémentation est totalement différente.

{{ IDEv ()}}


!!! tldr "Bilan de l'exemple"
    On a bien vu que le programmeur qui utilise une structure de données fait *abstraction* à la fois : 

    - de la manière dont les données sont représentées (ex. : l'écriture des instructions et les résultats sont les mêmes, que les données soient représentées par des couples ou des dictionnaires, ou autre chose...)  
    - de la manière dont les opérations sont programmées (ex. : le test d'égalité ne suit pas la même logique selon les deux implémentations mais le résultat est le même).



## 5. Synthèse


- Nous avons vu qu'une **structure de données** est une méthode de stockage et d'organisation des données destinée à en faciliter l'accès et la modification. Elle regroupe des *données* à gérer et un *ensemble d'opérations* qu'on peut leur appliquer.  
- Les structures de données s'envisagent à deux niveaux : l'interface (abstrait) et l'implémentation (concret).
    - L'**interface** est la spécification de l'ensemble des opérations de la structure de données. C'est la partie visible pour qui veut utiliser ce type abstrait de données. Elle est suffisante pour utiliser la structure de données.  
    - L'**implémentation** consiste à *concrétiser* - réaliser effectivement - un type de données en définissant la représentation des données avec des types de données existants, et en écrivant les programmes des opérations. L'utilisateur doit pouvoir écrire les mêmes instructions et obtenir les mêmes résultats quelle que soit l'implémentation de la structure de données.  
- On peut écrire plusieurs implémentations d'une même structure de données (d'une même interface). Le choix de l'implémentation est en général guidé par les opérations de la structure et leurs coûts pour un traitement algorithmique donné. Nous y reviendrons lorsque nous étudierons quelques structures de données classiques.
- Cette dissociation entre interface et implémentation est présente lorsque nous utilisons une bilbiothèque (ou plus tard dans l'année, une API) : nous n'avons pas besoin de connaître son implémentation pour l'utiliser, son interface suffit. Par exemple, la [documentation officielle](https://docs.python.org/fr/3.9/library/random.html) de la bibliothèque `random` présente la spécification des opérations disponibles (son interface) et nous suffit pour l'utiliser. Il est néanmoins possible de voir l'implémentation en cliquant en haut de la page sur le lien vers le code source.

--- 

références :


- [Germain BECKER, Lycée Mounier, ANGERS](https://info-mounier.fr/terminale_nsi/structures_donnees/index.php#chap1)
- [eskool nsi terminale](https://eskool.gitlab.io/tnsi/donnees/)
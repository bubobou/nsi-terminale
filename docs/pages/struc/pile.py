class Cellule :
    def __init__(self, contenu, suivante):
        self.contenu = contenu
        self.suivante = suivante

class Pile :
    def __init(self):
        self.data = None
    def est_vide(self):
        return self.data is None
    def empiler(self,x):
        c = Cellule(x,self.data)
        self.data = c
    def depiler(self):
        if self.est_vide():
            raise IndexError("la liste est vide")
        valeur = self.data.contenu
        self.data = self.data.suivante
        return valeur
        


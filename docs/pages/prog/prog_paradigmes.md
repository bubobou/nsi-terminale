---
hide:
  - footer
---

# Paradigmes de programmation

## 1. Différentes façon de programmer

Il existe de très nombreux langages de programmations (plus de 2000), et on peut 
légitiment se poser la question de l'intérêt de disposer d'autant de moyens de 
programmer. Pourquoi choisir un langage plutôt qu'un autre ?

Pourquoi ne pas avoir un unique langage de programmation? La raison
est qu'un langage est souvent conçu pour répondre à des besoins ou des
problématiques spécifiques. Par exemple, pour écrire un driver de carte graphique, 
il est parfois nécessaire de programmer en assembleur. Si l'on souhaite
effectuer des opérations sur une base de données on choisira plutôt le langage SQL.

Bien qu'il en existe un très grand nombre, les langages peuvent néanmoins être rassemblés 
par familles - on dit aussi paradigmes de programmation - selon les concepts qu'ils 
mettent en œuvre. 

Parmi les principaux paradigmes, on peut citer :

- Impératif
- Déclaratif
- Concurrent
- Orienté contraintes
- Orienté objets
- Événementiel
- Synchrone
- Fonctionnel
- Orienté requêtes
- Logique

Vous avez l'habitude d'utiliser le paradigme impératif, qui est celui des 
langages permettant de manipuler des structures de données modifiables 
(comme les variables, les tableaux, etc.) en utilisant notamment des boucles 
(while, for, etc.). En classe de première vous avez utilisé le langage HTML 
qui est déclaratif, souvent associé au langage JavaScript qui lui est orienté 
vers la gestion d'évènement. En terminale vous avez aussi découvert les concepts 
du paradigme orienté objets, qui sont liés aux notions de classes, de méthodes 
et d'héritage.

Enfin, il ne faut pas oublier l'apprentissage automatique qui révolutionne
la programmation depuis quelques années et qui va certainement induire de 
nouvelles façons de programmer.

Il faut tout de même avoir à l'esprtit que quel que soit le langage et le paradigme 
de programmation choisit, les instructions sont traduites en langages machines
qui lui est impératif.

## 2. Programmation fonctionnelle

Nous allons étudier un paradigme particulier : le paradigme fonctionnel. 
Celui-ci, comme son nom l'indique, utilise sur la notion de fonction, en 
s'appuyant sur la manipulation de données non modifiables.

## 2.1 Historique

La programmation fonctionnelle est apparue dans les années 50 avec *Lisp*, mais 
a plutôt été reservé à un usage académique. *Lisp* a donné naissance à des variantes 
telles que *Scheme* (1975) et *Common Lisp* (1984). Parmi les langages fonctionnels plus 
récents ont trouve *ML* (1973), *Haskell* (1987), *OCaml*, *Erlang*, *Clean* , *Scala* (2003)... 

Un langage tel que Python même s'il n'est pas purement fonctionnel, a néanmoins été
fortement influencé par ce paradigme.

## 2.2 Caractéristiques de la programmation fonctionnelle

Les langages fonctionnels ont comme propriété la transparence référentielle. Ce terme recouvre le principe simple selon lequel le résultat du programme ne change pas si on remplace une expression par une expression de valeur égale. Ce principe est violé dans le cas de procédures à effets de bord puisqu'une telle procédure, ne dépendant pas uniquement de ses arguments d'entrée, ne se comporte pas forcément de façon identique à deux instants donnés du programme. 

La mise en œuvre des langages fonctionnels fait un usage sophistiqué de la pile car, afin de s'affranchir de la nécessité de stocker des données temporaires dans des tableaux, ils font largement appel à la récursivité.

## 2.3 Fonctions passées en argument

## 2.4 Fonctions renvoyés comme résultat
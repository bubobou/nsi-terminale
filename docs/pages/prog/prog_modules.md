---
author : Valéry Bruniaux - Lycée Arthur Rimbaud, Aix-Marseille
hide:
  - footer
---

# Modularité


En informatique on est souvent amené à **ré-utiliser des fonctions déjà programmées** 
pour d'autres projets. Il faut donc programmer en pensant le code pour une ré-utilisation
future. C'est notamment pour cela qu'il faut bien documenter et tester son code afin qu'il
 soit utilisable plus tard et par d'autres.

Parfois aussi on utilise des fonctions présentes dans des **bibliothèques (ou librairies)**
afin d'accéder à des services mis à disposition par des applications, on appelle ça des 
**API (Application Programming Interface)**.

Pour toutes ces situations où on utilise des fonctions déjà existantes, on utilise des 
**modules** qui contiennent ces fonctions prêtes à être utilisées. Cette technique est
un aspect fondamental de la programmation informatique.


## 1. Utilisation d'un module

En Python, il existe 4 moyens **d'importer** des fonctions d'un module. Regardons des 
exemples autour du module `math` qui contient des fonctions mathématiques.


!!! example "Importer un module"

    Dans l'exemple ci-dessous, on déclare qu'on va utiliser les fonctions du module `math`.
    On accède aux fonctions (ici `gcd`) du module en les préfixant du nom du module.

    ``` python
    import math
    pgcd = math.gcd(35,25)
    ```

!!! example "Importer un module avec un alias"

    Pour éviter d'écrire à chaque fois entièrement le nom du module lors de l'appel
    d'une fonction, on peut renommer celui-ci en utilisant un aliasage.

    ``` python
    import math as m
    pgcd = m.gcd(35,25)
    ```

    Ici on a créé un alias nommé `m`pour le module `math`.

!!! example "Importer une fonction spécifique d'un module"

    Il est parfois inutile d'importer tout un module alors qu'on a besoin qu'une seule
    de ses fonctions. On spécifie alors le nom de la fonction lors de l'import.

    Dans ce cas, la fonction importée est accessible directement par son nom, sans 
    avoir a spécifié celui du module lors de l'appel.

    ``` python
    from math import gcd 
    pgcd = gcd(35,25)
    ```

!!! example "Importer toutes les fonctions d'un module"

    Si on souhaite importer toutes les fonctions d'un module de façon à y accéder 
    directement on utilise le caractère "joker" `*`.

    ``` python
    from math import *
    pgcd = gcd(35,25)
    ```

!!! warning "Pratique déconseillée"

    Cette dernière technique semble pratique mais en réalité elle est **à éviter** car 
    on importe énormément de fonctions inutiles qu'on ne connait pas, ce qui risque de 
    créer des conflits dans l'**espace global de nommage** et amener des bugs difficiles 
    à résoudre.



## 2. Accéder à la documentation

Dans la **console python** vous pouvez accéder à la **documentation (`docstring`) d'un module ou d'une fonction après avoir importé le module**. Essayez ceci dans la console python (`q` pour quitter l'aide) :

```python
    >>> import math
    >>> help(math)
    >>> help(gcd)
```

Quand vous écrivez une fonction ou un module, vos `docstring` peuvent être lues par ce moyen afin d'utiliser **correctement** la fonction. D'où la nécessité de bien **documenter votre code**.


## 3. Création d'un module


Il suffit d'écrire un programme python contenant vos fonctions. La **`docstring` du module se place au début du fichier**. Voici un exemple avec le module `vecteur`. 

``` python
# -*- coding: utf-8 -*-

""" Ce module contient des fonctions pour faire des calculs sur les vecteurs.
    Les vecteurs sont exprimés dans des tuples (x,y) """

def addition(v1: tuple, v2: tuple) -> tuple:
    """ vectors are tuple (x,y)
        return the vector v1 + v2 in a tuple
    """
    pass

def soustraction(v1: tuple, v2: tuple) -> tuple:
    """ vectors are tuple (x,y)
        return the vector v1 - v2 in a tuple
    """
    pass
```

- Enregistrer ce programme dans un fichier nommé `vecteur.py`  
- Se placer dans le répertoire du fichier créer.
- Taper dans la console python `import(vecteur)` puis `help(vecteur)` et enfin `help(addition)`.
- Écrire le code des 2 fonctions du module.


## 4. Implémentation et utilisation de notre module.

- Écrivez un programme `programme.py` dans le même dossier que `vecteur.py` qui :  
    - Importe le module `vecteur`  
    - Déclare 2 variables `v1` et `v2` en leur donnant des valeurs.  
    - Utilise les fonctions du module `vecteur` pour réaliser des opérations sur `v1` et `v2`.  

- Complétez le module `vecteur` en ajoutant les opérations suivantes :  
    - Calcul de la norme.  
    - Multiplication d'un vecteur par un nombre.  
    - Division d'un vecteur par un nombre.
    - Calcul du produit scalaire.  
    - Calcul de l'angle entre deux vecteurs

## 6. Gestion des exceptions

Comme évoqué dans le cours sur interface et implémentation, manipuler les fonctions fournies 
par une interface permet de ne pas se soucier des détails d'implémentation. Néanmoins, cela 
ne nous garantit pas un fonctionnement sans problème.

Dans notre module, nous utilisons des vecteurs dans le plan créer par des tuples de deux valeurs,
que ce passe-t-il si l'utilisateur malgré les informations de la docstring, utilise `addition`
 avec deux entiers ?

Une bonne pratique consiste, lors du développement de nos modules, à renvoyer à l'utilisateur 
des erreurs explicites, qui peuvent être interprétées à l'aide de la seule connaissance de 
l'interface.

### 6.1 Lever une exception

Nous avons déjà pu observer à de nombreuses reprises des programmes
s'interrompre avec des messages d'erreurs variés que nous étudierons plus en détail dans le
prochain chapitre. 

Il faut savoir qu'il est possible de déclencher une exception avec l'opération `raise`, 
on dit qu'on ***lève une exception***.

Cette opération s'écrit en faisant suivre le mot-clé `raise` du nom de l'exception 
à lever, lui-même suivi entre parenthèses d'une chaîne de caractères permettant d'écrire 
un message personnalisé sur l'erreur signalée.

!!! example "Exemple de levée d'exception"

    Dans le cas du parcours d'une séquence, on peut ainsi prévoir ce message
    en cas de dépassement d'indice.

    ```python
    raise IndexError('indice trop grand')
    ```



### 6.2 Rattrapper une exception

Certaines exceptions traduisent des erreurs du programme: elles sont imprévues 
et leurs conséquences ne sont par définition pas maîtrisées. Dans ces conditions,
interrompre l'exécution du programme est légitime. 

D'autre exceptions, en revanche, s'inscrivent dans le fonctionnement normal 
du programme: elles correspondent à des situations connues, exceptionnelles 
mais possibles et donc prévisibles.

Dans ce cas il est possible d'anticiper le comportement du programme et de prévoir 
une alternative.Pour cela il faut l'intercepter avant que l'exécution du programme
ne soit abandonné. On dit que l'on rattrape une exception.

La levée d'une exception se fait en utilisant les mots clés `try` et `except`.

!!! example "Example d'utilisation de `try` ... `except`"

    ```python
    try:
        x = int (input ("Entrer un jour"))
    except ValueError:
        print("Prière d'entrer un entier valide")
    ```

---
!!! Abstract "Sources et bibliographie"

    - Valery Bruniaux, Lycée Arthur Rimbaud, Istres
    - Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.


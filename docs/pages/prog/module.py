def division(v:tuple,k:int)-> tuple :
    try :
        return (v[0]/k,v[1]/k)
    except ZeroDivisionError:
        print("k ne peut pas être nul")
    except TypeError:
        print("calcul impossible erreur de type de donnée")

v=(4,2)
k=0

print(division(v,k))
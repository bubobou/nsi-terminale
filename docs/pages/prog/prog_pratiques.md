---
hide:
  - footer
---

# Mise au point de programmes

!!! quote "Éléments du référentiel"

    **Contenus** : 
    - Mise au point des programmes. 
    - Gestion des bugs.

    **Attendus**:
    Dans la pratique de la programmation, savoir répondre aux causes typiques de bugs :
    - problèmes liés au typage, effets de bord non désirés,  
    - débordements dans les tableaux,  
    - instruction conditionnelle non exhaustive,  
    - choix des inégalités,  
    - comparaisons et calculs entre flottants,  
    - mauvais nommage des variables, etc.

## 1. Identifier les erreurs

Les messages d'erreurs que nous pouvons observés lors d'un problème durant
l'exécution d'un message permettent d'identifier ce que l'on nomme *exception*
en programmation.

Le tableau ci-dessous donne quelques exceptions courantes, observables
lors de l'exécution d'un programme en Python.

| Exception | Contexte |
|:----------|:----------------------------|
|NameError  | accès à une variable inexistante |
|IndexError | accès à une variable inexistante
|KeyError   | accès à une clé inexistante d'un dictionnaire |
|ZeroDivisionError | division par zéro |
|TypeError  | opération appliquée à des valeurs incompatibles |


!!! note "Pile d'appels"

    Le message affiché lorsqu'une exception interrompt un programme renseigne 
    sur l'état de la pile d'appels au moment où l'exception a été levée. 
    On peut y lire ainsi quelle première fonction a appelé quelle deuxième fonction 
    qui a, à son tour, appelé quelle autre fonction, etc., jusqu'à arriver au 
    point où l'exception a été levée.

    Il s'agit d'une information cruciale pour comprendre le contexte du
    problème et tenter le corriger.


## 2. Typage

Les types permettent de caractériser les opérandes ou paramètres qui sont ou non 
acceptables pour certaines opérations. En particulier, l'utilisation de valeurs 
qui seraient par nature incompatibles avec une opération donnée lève en Python 
une exception `TypeError` qui est généralement accompagnée d'information sur 
les types qui ne conviennent pas.

!!! example "Exemple d'erreur de type"

    L'addition d'un entier et d'un tableau n'est pas possible en Python.

    ```
    >>> 1 + [2, 3]
    Traceback (most recent calI last):  File Il <stdin> Il , line 1, in <module>
    TypeError: unsupported operand type(s) for +: 'int' and 'list'
    ```

En Python, la gestion des types est qualifiée de dynamique : c'est au
moment de l'exécution du programme, lors de l'interprétation de chaque
opération de base, que l'interprète Python vérifie la concordance entre les
opérations et les types des valeurs utilisées.

### 2.1 Annoter les types

Il est indispensable lors de la définition d'une fonction d'avoir en tête les types 
attendus pour les paramètres et l'éventuel type du résultat. C'est une information 
cruciale à fournir dans l'interface d'un module, pour préciser la description (docstring)
de chaque fonction et éviter autant que possible leur mauvaise utilisation.

Pour inclure ces informations sur les types dans le programme lui-même il est 
possible d'annoter le code d'un programme, et en particulier les définitions de 
variables et de fonctions.

Ainsi, lors de la première définition d'une variable `x` destinée à représenter un 
nombre entier on peut utiliser la notation suivante :

```python
x : int = 42
```

!!! example "Annotation de type dans une fonction"

    La fonction `cree` n'attend aucun paramètre et renvoie un tableau.

    ```python
    def cree () -> list :
    ```

    La fonction `contient` attend pour paramètres un tableau et un entier, et
    renvoie un booléen. Les deux paramètres annotés sont séparés par une virgule, 
    comme ils l'étaient avant d'être annotés.

    ```python
    def contient(s: list, x: int) -> bool:
    ```

    La fonction `ajoute` attend pour paramètres un tableau et un entier, et ne
    renvoie pas de résultat. On note l'absence de résultat en indiquant `None`
    comme type de retour.

    ```python
    def ajoute(s: list, x: int) -> None:
    ```

## 2.2 Alias de types

Lorsque l'on manipule des structures de données particu lières, il est courant de 
fournir un nouveau nom (appelé alias) à leur type. Ainsi, la structure de données 
utilisée comme exemple précédemment pourrait être simplement nommée `Ensemble` dans
l'interface du module correspondant, et les trois fonctions auraient alors les
types suivants.

```python
cree() -> Ensemble
contient(s: Ensemble, x: int) -> bool
ajoute(s: Ensemble, x: int) -> None
```

Cette pratique présente certains intérêts :

- elle présente à l'utilisateur du module un nom significatif, décrivant ce que 
 l'objet fourni représente et non la manière dont il est réalisé en interne. 
- elle accompagne l'encapsulation : le type présenté par l'interface ne dépend 
 pas des détails d'implémentation, et n'a donc pas à être modifié en cas d'évolution
 de la représentation interne.
    

## 3. Écrire des tests

Plutôt que de prouver la justesse d'un programme, qui est parfois peu aisée à montrer,
on se contente bien souvent de tester  le code en vérifiant  qu'il se comporte correctement
sur des entrées pour lesquelles le résultat est connu.

### 3.1 Tests unitaires

Les tests unitaires ont pour but de tester indépendamment chaque fonction d'un
programme.

Dans l'idéal ces tests sont conçus avant même les fonctions à tester : on parle 
d'*approche de de développement par les tests* ou *TDD* (en anglais : *Tests Driven Development*). 

Ils sont réalisés le plus souvent possible lors du développement, pour éviter les problèmes 
de regression lors d'ajout de nouvelles fonctionnalités ou de modification.

Ces tests doivent être le plus couvrants possibles afin d'envisager tous les cas de figures.

### 3.2 Outils de test

#### 3.2.1 Assertion

Une assertion permet de vérifier qu'un fonction renvoie bien le résultat attendu pour
une entrée donnée.

En Python, on utilise le mot clé `assert`, suivi de l'expression à tester.
Dès qu'une assertion est fausse, l'exécution du programme s'arrête.

!!! example "Tester une fonction de tri"

    Supposons que l'on dispose d'une fonction `tri` qui prend en argument un 
    tableau d'entiers et renvoie ce tableau trié.

    On peut alors écrire ces assertions :

    ```python
    assert tri([1,3,2]) == [1,2,3]
    assert tri([]) == []
    assert tri([0,-1,7,18]) == [-1,0,7,18]
    ```

#### 3.2.2 Doctest

Le module `doctest`permet d'intégrer des tests directement dans la *docstring* 
des fonctions. Les tests sont précédés par la chaîne ">>>".

!!! example "documenter une fonction de tri"

    On peut alors écrire les assertions précédentes au sein de la fonction `tri`:

    ```python
    def tri(tab : list) -> list:
        """trie un tableau d'entier par ordre croissant et renvoie le tableau trié
        >>>tri([1,3,2])
        [1,2,3]
        >>>tri([])
        []
        >>>tri([0,-1,7,18])
        [-1,0,7,18]
        """
        ...
    ``` 

Ces tests peuvent être validés depuis l'interpréteur Python de la façon suivante :

```
>>>import doctest
>>>doctest.testmode(tri)
```

#### 3.2.3 Pytest

Le module `pytest` permet de faire des tests plus élaborés, en utilisant notamment
des fonctions spécifiquement dédiées aux tests. Pour ce faire on écrit des fonctions
préfixées par le mot `test_` et complétées généralement par le nom de la fonction que
l'on souhaite testée.

!!! example "Écriture d'une fonction de test"

    Si l'on reprend notre fonction de tri :

    ```python
    def test_tri(t):
        tri(t)
        for i in range (0, len (t) - 1):
            assert tri] <= t[i+1]
    ```

    Que l'on peut compléter avec une fonction permettant de créer un tableau aléatoire.

    ```python
    from random import randint
    def tableau_aleatoire(n, a, b):
        return [a + randint(0, b - a) for _ in range(n)]
    ```



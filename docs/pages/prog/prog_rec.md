---
hide:
  - footer
---

# Récursivité

source : ce cours s'appuie en grande partie sur [celui de Gilles Lassus](https://glassus.github.io/terminale_nsi/T2_Programmation/2.2_Recursivite/cours/). 

## 1. Partons d'un bon pied

Comme le font chanter aux enfants les moniteurs de colonie de vacances pour les encourager
à avancer à pied : "la meilleure façon de marcher c'est encore la notre : **c'est de mettre un pied devant l'autre et de recommencer** ".

D'un point de vue algorithmique, on peut considérer cela comme une façon itérative de décrire la fonction
 *marcher*, à savoir en répétant un certains nombre de fois (ici indéfini) la même action (mettre un pied 
devant l'autre).

```
fonction Marcher():
        Tant que ?? faire:
            Mettre un pied devant l'autre
        FinTantQue
```

Bien entendu vous remarquez qu'il manque une information cruciale dans la boucle `Tant que`, mais la 
chanson ne parle pas de la destination, donc on ce contentera pour l'instant de ce formalisme.

Considérons maintenant une autre façon de définir la fonction *marcher* :

```
fonction MarcherBis():
        Mettre un pied devant l'autre
        MarcherBis()
```

Cette fonction permet d'avancer, tout aussi bien que la précédente et bien entendu là aussi nous avons
un problème de destination et plus précisemment d'arrêt (nous en parlerons plus tard).

Ce type de définition fonctionne tout à fait en informatique, le code ci-dessous est parfaitement valide
bien que totalement déconseillé.

```python
    def mauvais_exemple():
        print("il ne faut pas programmer comme ça")
        mauvais_exemple()
```

Si on appelle cette fonction dans la console :

```python
>>> mauvais_exemple()
```

La sortie sera celle-ci ...

```
il ne faut pas programmer comme ça
il ne faut pas programmer comme ça
il ne faut pas programmer comme ça
il ne faut pas programmer comme ça
il ne faut pas programmer comme ça
il ne faut pas programmer comme ça
il ne faut pas programmer comme ça
.
.
.
```

... et l'on devra interrompre manuellement le programme car il n'y a aucune raison qu'il s'arrête de lui même.


C'est ce qu'il faudra absolument éviter en programmation (à quelques rares exception près) : tomber dans le piège de
la boucle infinie.

![boucle infinie](https://media.giphy.com/media/3ov9jQX2Ow4bM5xxuM/giphy.gif){width=20%}


## 2. Définir la récursivité

### 2.1 Définition

!!! note "Fonction récursive"
    Une fonction est dite récursive lorsqu'elle fait appel à elle même dans sa propre définition.

Si vous faites une recherche à propos de ce mot, vous pourrez obtenir une définition dans ce genre :

> **Fonction récursive** : fonction qui fait appel à la récursivité. Voir *fonction récursive*.

### 2.2 Récursivité & culture informatique

Dans la culture informatique, les références à la récursivité en terme de définition auto-référencé sont
monnaie courante et constituent bien souvent des blagues d'initié-e-s.

Par exemples les [acronymes récursifs](https://fr.wikipedia.org/wiki/Sigles_auto-r%C3%A9f%C3%A9rentiels){target=_blank} sont très fréquents... et véhiculent avec eux le même piège : une fonction récursive ne serait *jamais vraiment définie* (c'est faux, nous le verrons).

Par exemple :

- GNU (dans GNU/Linux) signifie "GNU is Not Unix". On ne sait jamais vraiment ce que signifie GNU...  
- LINUX signifie : "LINUX Is Not UniX"
- PHP (le langage serveur) signifie "PHP: Hypertext Preprocessor"
- VISA (les cartes bancaires) signifie "VISA International Service Association".

### 2.3 L'arrêt de la récursion

Disons-le clairement : au-delà de la blague pour initiés (dont vous faites partie maintenant) la récursivité ne DOIT PAS 
être associée à une auto-référence vertigineuse : c'est en algorithmique une méthode (parfois) très efficace, à condition 
de respecter une règle cruciale :  :star: :star: :star: **l'existence d'un CAS DE BASE** :star: :star: :star: .  

Ce «cas de base» sera aussi appelé «condition d'arrêt», puisque la très grande majorité des algorithmes récursifs peuvent 
être perçus comme des escaliers qu'on descend marche par marche, jusqu'au sol qui assure notre arrêt.

Les exemples que nous avons pris en première partie ne disposaient pas de cas de base : on ne sait toujours pas
quand on doit s'arrêter de marcher...

## 3. Utiliser la récursivité

### 3.1 Illustration en BD

![BDGLassus](https://glassus.github.io/terminale_nsi/T2_Programmation/2.2_Recursivite/data/bd.png){width=60%}

Observez bien la descente puis la remontée de notre vendeur de livre. 
Le cas de base est ici l'étage 0. Il empêche une descente infinie.

Nous coderons bientôt la fonction donnant le prix du livre en fonction de l'étage.

### 3.2 Un exemple qui fonctionne

Une fonction récursive bien écrite ressemble par exemple à celle-ci :

!!! note "Exemple fondateur n°1 "
    ```python linenums='1'
    def mystere(n):
        if n == 0 :
            return 0
        else : 
            return n + mystere(n-1)
    ```


Trois choses sont essentielles et doivent se retrouver dans tout programme récursif :

- ```lignes 2 et 3``` :  le cas de base (si ```n``` vaut 0 on renvoie *vraiment* une valeur, en l'occurence 0)
- ```ligne 5``` : l'appel récursif
- ```ligne 5``` : la décrémentation du paramètre d'appel


**Utilisation de la fonction ```mystere```** 

```python
>>> mystere(0)
0
>>> mystere(4)
10
```



!!! aide "Analyse grâce à PythonTutor"
    <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20mystere%28n%29%3A%0A%20%20%20%20if%20n%20%3D%3D%200%20%3A%0A%20%20%20%20%20%20%20%20return%200%0A%20%20%20%20else%20%3A%20%0A%20%20%20%20%20%20%20%20return%20n%20%2B%20mystere%28n-1%29%0A%0Amystere%284%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>


Que se passe-t-il lorsqu'on appelle ```mystere(4)``` ?

```
    mystere(4)  = 4 + mystere(3)
                = 4 + ( 3 + mystere(2))
                = 4 + ( 3 + ( 2 + mystere(1)))
                = 4 + ( 3 + ( 2 + ( 1 + mystere(0))))
                = 4 + ( 3 + ( 2 + ( 1 + 0 )))

```

La succession des appels pourrait s'illustrer ainsi :

![illustration](https://glassus.github.io/terminale_nsi/T2_Programmation/2.2_Recursivite/data/diag.png)

On voit que l'existence du cas de base pour $n=0$ est primordiale pour éviter la récursion infinie.

??? question "Mais que fait donc cette fonction mystère ?"
    Cette fonction ```mystere(n)``` calcule la somme des entiers positifs inférieurs ou égaux à $n$.


### 3.3 Intérêt de la récursivité

Si l'on reprend l'exemple de la somme des *n* premiers entiers, on aurait pour réflexe de la définir en écrivant
la formule suivante  : `0+1+2+...+n`

Cette définition paraît simple et intuitive, mais comment programmer une fonction `somme(n)` de prime abord en Python ? 
Une première solution possible est d'utiliser une boucle `for` pour exprimer le caractère répétitif de cette fonction.

Par exemple :

```python
def somme(n):
    r = 0
    for i in range(n + 1):
        r = r + i
    return r
```

Si la fonction proposée permet bien de calculer la somme des *n* premiers entiers, il n'est pas évident de voir
que le code de celle-ci se rapporte à la formule initiale (`0+1+2+...+n`). Par exemple, rien dans cette formule ne 
laisse deviner qu'une variable intermédiaire *r* est nécessaire pour calculer cette somme.

Si d'un point de vue mathématique, nous définissons la somme des *n* premiers entiers de la façon suivante :

> *somme(n)* = 0 si *n* = 0 | *n* + *somme(n-1)* si *n* > 0

Cette définition correspond exactement à celle de la fonction `mystere` définie en 3.2

On voit l'intérêt de définir récursivement des fonctions : cela permet d'obtenir des définitions
qui seront directement transposables en programmation et qui de plus éliminerons toute ambiguité.


## 4. Programmer de façon récursive

La programmation récursive nécessite _outre une définition correcte des cas de base et des cas récursif_ de 
prendre en compte deux points importants :  

- Le domaine mathématique d'une fonction (les valeurs sur lesquelles elle est définie) n'est pas toujours le même que
l'ensemble des valeurs du type Python avec lesquelles elle sera appelée.  
- Le modèle d'exécution des fonctions récursives au sein de l'ordinateur n'est pas sans conséquence sur leur efficacité.

### 4.1 Type de données

Dans l'exemple de la fonction `mystere(n)` définie en 3.2, on peut ainsi se poser les questions suivantes : 

- que se passe-t-il si la fonction est appelée avec un entier négatif ?
- que se passe-t-il si la fonction est appelée avec un flottant ?

Par exemple, bien que la fonction mathématique *somme(n)* ne soit pas définie pour
*n* = -1, l'appel `mystere(-1)` ne provoque aucune erreur immédiate, mais il
implique un appel à `mystere(-2)`, qui déclenche un appel à `mystere(-3)`, etc.

Il y a plusieurs façons d'éviter ce problème, notamment en modifiant la condition du cas de base, ou en 
indiquant judicieusement des assertions dans le code.

### 4.2 Modèle d'exécution

Lors d'un appel à une fonction récursive, le processeur utilise une structure de **pile** pour stocker les contextes 
d'exécution de chaque appel. En classe de première, vous avez appris qu'une partie de l'espace mémoire d'un programme est 
organisée sous forme d'une **pile** où sont stockés les contextes d'exécution de chaque appel de fonction.

La pile d'appels de notre fonction ```mystere(5)``` peut donc être schématisée comme ceci :

![pile](https://glassus.github.io/terminale_nsi/T2_Programmation/2.2_Recursivite/data/pile_exec.gif){width=30%}

Nous venons de voir que notre appel à ```mystere(5)``` générait une pile de hauteur 6 (on parlera plutôt de *profondeur* 6). Cette profondeur est-elle limitée ?


```python
mystere(2962)
```


    ---------------------------------------------------------------------------

    RecursionError                            Traceback (most recent call last)

    <ipython-input-32-a97c4dde4ef8> in <module>
    ----> 1 mystere(2962)
    

    <ipython-input-1-386660a434f2> in mystere(n)
          3         return 0
          4     else :
    ----> 5         return n + mystere(n-1)
    

    ... last 1 frames repeated, from the frame below ...


    <ipython-input-1-386660a434f2> in mystere(n)
          3         return 0
          4     else :
    ----> 5         return n + mystere(n-1)
    

    RecursionError: maximum recursion depth exceeded in comparison


Vous venons de provoquer un «débordement de pile», le célèbre **stack overflow**. 

De manière générale, les programmes récursifs sont souvent susceptibles de générer un trop grand nombre 
d'appels à eux-mêmes. Il est parfois possible de les optimiser, comme nous le verrons dans le cours concernant 
la **programmation dynamique**.  

Nous reparlerons aussi de récursivité lorsque nous l'inscrirons dans un paradigme plus global de programmation, 
qui est **« diviser pour régner »** (en anglais *divide and conquer*), mais aussi dans le cadre de l'étude sur 
les **structures de données**.  


## 5. Définition récursives avancées

Toute formulation récursive d'une fonction possède au moins un cas de
base et un cas récursif. Néanmoins, il existe une grande variété de formes 
possibles.

### Cas de base multiple

Ajouter des cas de bases permet parfois d'éviter certains calculs inutiles.

### Cas récursifs multiples

Il est également possible de définir une fonction avec plusieurs cas récursifs. Nous verrons plus tard
que cela permet notamment d'implémenter un calcul de puissance efficace.

### Double récursion

Les expressions qui définissent une fonction peuvent aussi dépendre de plusieurs appels 
à la fonction en cours de définition.

C'est le cas du calcul des termes de la célèbre suite de Fibonacci :


- $F_0 = 0$
- $F_1 = 1$
- $\forall n \in \mathbb{N}, F_{n+2} = F_{n+1}+F_n$

On a donc $F_2=0+1=1, F_3=F_2+F_1=1+1=2, F_4=F_3+F_2=2+1=3, F_5=F_4+F_3=3+2=5$ ...

### Récursion imbriquée

Les occurrences de la fonction en cours de définition peuvent également être imbriquées.


### Récursion mutuelle

Il est parfois nécessaire de définir plusieurs fonctions récusives en parallèle qui s'appellent mutuellement.
On parle alors de définitions *récursives mutuelles*.

## 6. Efficacité de la récursivité



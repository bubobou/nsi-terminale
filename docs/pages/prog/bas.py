class Jeu:
    def __init__(self, nom_de_carte):
        carte = open(nom_de_carte)
        self.grotte = [[Case(char) for char in ligne if char != '\n'] for ligne in carte.readlines()]
        carte.close()
        self.lemmings = []

    def __getitem__(self, i):   # redéfinition de la méthode __getitem__ pour renvoyer vers le tableau 'grotte'
        return self.grotte[i]   # lorsqu'on parcourt un objet de classe Jeu (voir méthode déplacement)

    def affiche(self):
        for ligne in self.grotte:
            for case in ligne:
                print(case, end="")
            print ()
        print('\n')

    def ajoute_lemming(self):
        if self.grotte[0][1].libre():
            lem = Lemming(self, 0, 1)
            self.lemmings.append(lem)
            self.grotte[0][1].arrivee(lem)

    def tour(self):
        for lem in self.lemmings :
            lem.action()

    def demarre(self):
        while True:
            cmd = input()
            if cmd == 'q' :
                break
            elif cmd == 'l' :
                self.ajoute_lemming()
            else:
                self.tour()
            self.affiche()


class Lemming:
    def __init__(self, j, l, c):
        self.jeu = j
        self.ligne = l
        self.col = c
        self.dep = 1

    def __str__(self):
        if self.dep > 0:
            return '>'
        else:
            return '<'

    def deplacement(self, l, c):
        if not self.jeu[l][c].libre():
            return False
        self.jeu[self.ligne][self.col].depart() # la méthode __getitem__ de la classe Jeu
        self.jeu[l][c].arrivee(self)            # permet ici de ne pas écrire jeu.grotte[l][c]
        return True

    def action(self):
        if self.deplacement(self.ligne+1, self.col):
            self.ligne += 1
        elif self.deplacement(self.ligne, self.col + self.dep):
            self.col += self.dep
        else:
            self.dep = -self.dep

    def sort (self) :
        self.jeu.lemmings.remove(self)

class Case:
    def __init__(self, char):
        self.terrain = char
        self.lem = None
    def __str__(self):
        if self.lem is not None:
            return str(self.lem)
        else:
            return self.terrain
    def libre(self):
        return (self.terrain == ' ' or self.terrain == '0') and self.lem == None

    def depart(self):
        self.lem = None

    def arrivee(self, lem):
        if self.terrain == '0':
            lem.sort()
        else :
            self.lem = lem

Jeu('carte.txt').demarre()


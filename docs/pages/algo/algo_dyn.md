---
hide:
  - footer
---

# Programmation dynamique

La programmation dynamique est une technique pour améliorer l'efficacité 
d'un algorithme en évitant les calculs redondants.

Lors d'utilisation de fonctions récursives, par exemple, il n'est en effet pas 
rare d'observer une redondance des résultats intermédiaires.

## 1. Exemple de redondance avec Fibonacci

Partons d'une fonction `fibo()` définie de façon récursive par :

```python
def fibo(n):
  if n <= 1:
    return n
  return fibo(n-1) + fibo(n-2)
```

En lançant `fibo(5)` nous obtenons l'arbre des appels suivant :

![fibo5](fibo5.png){width=60%}

0ù l'on constate déjà que de nombreux appels sont redondants, notamment
`fibo(1)` qui est appelé 5 fois. Nous allons étudier deux approches liées 
à la programmation dynamique qui permettent d'améliorer l'efficacité de 
l'algorithme.

## 2. Démarche bas vers haut

La démarche *bottom-up* est une démarche itérative consistant à résoudre
les problèmes de petites tailles, jusqu'à la taille voulue en stockant les
résultats intermédiaires.

!!! example Fibonacci dynamique

    Pour le calcul des termes de la suite de fibonacci, on peut ainsi appliquer
    ce principe :

    ```python
    def fibo_dyn(n):
      liste = [0]*(n+1)
      liste[1] = 1
      for i in range(2,n+1):
        liste[i] = liste[i-1] + liste[i-2]
      return liste[n]
    ```



## 3. Mémoïsation

Une autre technique consiste à utiliser un dictionnaire, dans lequel on 
stocke les calculs déjà effectués, on appelle cela la **mémoïsation**.

Ainsi, pour effectuer le calcul de $f(a)$, on commence par regarder dans 
le dictionnaire, indexé par $a$, si la valeur 
de $f(a)$ est déjà connue. Le cas échéant, on la renvoie. Sinon, on calcule la 
valeur $v$ de $f(a)$, on remplit le . dictionnaire avec $a \mapsto v$, puis on 
renvoie $v$. Ainsi, si on cherche plus tard à recalculer $f$ pour la même valeur
 $a$, alors le calcul sera immédiat.

!!! info Fibonacci avec mémoïsation

    Implémenter le calcul du n-ième terme de la suite de Fibonacci en utilisant
    le principe de la mémoïsation en utilisant pour cela un dictionnaire.

    {{ IDE()}}


## 4. Alignement de séquences

Une séquence est une suite de caractère d'un alphabet donné. Le problème
de l'alignement de séquences consiste à mettre en correspondance le plus
possible de caractère entre deux chaînes.

Ce type de problème est particulièrement rencontré en biologie lorsqu'il
s'agit de comparer des séquences d'ADN.

### 4.1 Principe

Pour explorer toutes les possibilités d'alignement entre deux séquences 
ont utilise un *trou* (*gap* en anglais), représenté par le caractère `-`.

Pour effectuer un alignement, on doit respecter les contraintes suivantes :

- on doit utiliser tous les caractères de chaque chaînes dans l'ordre
- on n'aligne jamais deux trous

### 4.2 Score d'un alignement

Parmi tous les alignements possibles on recherche celui qui donne le *score*
maximal. Le score est calculé de la façon suivante :

- l'alignement de deux caractères identiques augmente le score de 1
- l'alignement de deux caractères différents diminue le score de 1

!!! example "Exemple de calcul de score"

    Considérons les chaînes `GENOME` et `ENORME` :

    - pour l'alignement :  
      `GENO-ME`  
      `-ENORME`  
      le score est de : -1 + 1 + 1 + 1 - 1 + 1 + 1 = 3

    - l'alignement :  
      `G---ENOME`  
      `-ENO--RME`  
      le score est de : -1-1-1-1-1-1-1+1+1 = -5 

### 4.3 Recherche du meilleur score

De même que pour le calcul des termes de la suite de Fibonacci
nous pouvons trouver une solution récursive à ce problème, et là
aussi nous serons confrontés à la redondance de calculs et donc
à un algorithme peu efficace.

Dans le cas de l'alignement de séquence nous pouvone utiliser un 
tableau à deux dimensions dans lequel on va stocker le score maximal
pour chaque paire de mots.

!!! note "Algorithme d'alignement de séquences"

    Dans l'algorithme proposé ci-dessous :

    - on commence par initialiser un tableau `score` de dimension 
     (n1 + 1) × (n2 +1), où n1 et n2 sont les longueurs des séquences.     
    - on initialise les valeurs score[i][0] et score[0][j] correspondant
     à l'alignement avec un mot vide.  
    - on remplit le reste du tableau avec une double boucle en considérant 
      les trois cas suivants selon l'alignements des derniers caractères.
  

    ```python
    def aligne(s1 : str, s2 : str) -> int :
      """renvoie le score du meilleur alignement de s1 et s2"""
      n1, n2 = len(s1), len(s2)
      score = [[0]*(n2+1) for _ in range(n1+1)]
      for i in range(1, n1+1) :
        score[i][0] = -i
      for j in range(1, n2+1) :
        score[0][j] = -j
      for i in range(1, n1+1):
        for j in range(1,n2+1):
          s = max(-1 + score[i-1][j], -1 + score[i][j-1])
          if s1[i-1]  == s2[j-1]:
            score[i][j] = max(s, 1 + score[i-1][j-1])
          else :
            score[i][j] = max(s, -1 + score[i-1][j-1])
      return score[n1][n2]

    ```
def fibo_dyn(n):
      liste = [0]*(n+1)
      liste[1] = 1
      for i in range(2,n+1):
        liste[i] = liste[i-1] + liste[i-2]
      return liste[n]

def fibo_mem(n:int) -> int:
  dico = {0:0,1:1}
  for i in range(2,n+1):        
        dico[i] = dico[i-2]+dico[i-1]
  return dico[n]

print(fibo_mem(10))


---
hide:
  - footer
---

# Recherche textuelle

## Introduction

En *algorithmique du texte*, l'un des grands problèmes est la **recherche de motif dans un texte**.

Concrètement, rechercher le motif `M` dans un texte `T` consiste à rechercher toutes les occurrences de `M` dans `T`.

!!! example "Exemple"

    Si M = GCAG et T = GGCAGCCGAACCGCAGCAGCAC alors M 
    apparaît 3 fois dans T, aux positions 1, 12 et 15 :

    <p style="text-align: center">
    G<span style="text-decoration: underline; background-color: #D5F5E3;">GCAG</span>CCGAACC<span style="text-decoration: underline; background-color: #D5F5E3;">GCA</span><span style="text-decoration: underline overline; background-color: #D5F5E3;">G</span><span style="text-decoration: overline; background-color: #D5F5E3;">CAG</span>CAC
    </p>

**Applications fréquentes** :

- Recherche d'un mot dans un document (le célèbre CTRL+F)

- Recherche d'un mot sur le Web par les moteurs de recherche

- Recherche d'une sous-séquence d'intérêt dans une séquence biologique


## 1. Recherche par fenêtre glissante

Les différents algorithmes de recherche exacte de motif sont basés sur une recherche par *fenêtre glissante* dont l'idée est la suivante :

- Positionner le motif M à différentes positions de T
- Pour chaque position <span style="font-family: Consolas">i</span> choisie, tester si M apparaît dans T (c'est-à-dire si <span style="font-family: Consolas">M[0..m-1] = T[i..i+m-1]</span> ?)
- Décaler M (changement de position dans T) et recommencer

!!! example "Exemple"

    <pre style="font-size: 15px; line-height: 20px;">
    <span style="color: #aaa;">i   0 1 2 3 4 5 6 7 8 9 ... </span>
    T = G <span style="background-color: #D5F5E3;">G C A G</span> C C <span style="background-color: #F5B7B1;">G A A C</span> C <span style="background-color: #D5F5E3;">G C A </span><span style="background-color: #D5F5E3;">G</span> C A G</span> C A C
            &#10004;&#65039;          &#10060;        &#10004;&#65039;
    M     <span style="background-color: #AED6F1;">G C A G</span>     
                &xrarr;    <span style="background-color: #AED6F1;">G C A G</span>
                            &xrarr;  <span style="background-color: #AED6F1;">G C A G</span>
                                    &xrarr; ...
    </pre>

## 2. Recherche naïve

!!! example "Illustration"

    <iframe width="800" height="255" frameborder="0" src="https://germainbecker.github.io/recherche-textuelle/iframe-embed.html?t=GGCAGCCGAACCGCAGCAGCAC&m=GCAG"></iframe>

### 2.1 Premier algorithme

!!! note "Algorithme de recherche naïve :heart:"

    ```python linenums='1'
    def recherche_naive(texte, motif):
        '''
        renvoie la liste des indices (éventuellement vide) des occurrences de
        de la chaîne motif dans la chaîne texte.
        '''
        indices = []
        i = 0
        while i <= len(texte) - len(motif):
            k = 0
             while k < len(motif) and texte[i+k] == motif[k]:
                k += 1
            if k == len(motif):
                indices.append(i)
            i += 1

        return indices
    ```


### 2.2 Modification de l'algorithme

!!! abstract "Exercice 1"

    === "Énoncé"
        Re-écrire l'algorithme précédent en s'arrêtant dès qu'une occurrence de ```motif``` est trouvée dans ```texte```.

        La fonction renverra uniquement un booléen. 

    === "Correction"
        ```python linenums='1'
        def recherche_naive_bool(texte, motif):
            '''
            renvoie un booléen indiquant la présence ou non de
            la chaîne motif dans la chaîne texte.
            '''
            trouve = False
            i = 0
            while i <= len(texte) - len(motif) and not trouve:
                k = 0
                while k < len(motif) and texte[i+k] == motif[k]:
                    k += 1
                if k == len(motif):
                    trouve = True
                i += 1

            return trouve
        ```
         

### 2.3 Application à la recherche d'un motif dans un roman

Le [Projet Gutenberg](https://www.gutenberg.org/browse/languages/fr){. target="_blank"} permet de télécharger légalement des ouvrages libres de droits dans différents formats.

Nous allons travailler avec le Tome 1 du roman _Les Misérables_ de Victor Hugo, à télécharger [ici](data/Les_Miserables.txt){. target="_blank"} au format ```txt```. 

#### 2.3.1 Récupération du texte dans une seule chaîne de caractères

```python linenums='1'
with open("Les_Miserables.txt") as f:
    texte = f.read().replace('\n', ' ')
```

#### 2.3.2 Vérification et mesure du temps de recherche

!!! abstract "Exercice 2"
    === "Énoncé"
        1. Testez la validité de vos réponses en comparant avec les résultats donnés par la fonctionnalité ```Ctrl-F``` proposée par votre navigateur
        2. Mesurez le temps d'exécution de votre algorithme à l'aide du module ```time```.  
    === "Correction"
        *à faire*
         
### 2.4 Coût de l'algorithme naïf

Pour évaluer le coût des algorithmes de recherche d'un motif dans un texte on se base sur le **nombre de comparaisons** nécessaires entre un caractère du motif et un caractère du texte.

Des données de la forme T = <span style="font-family: Consolas; font-size: 15px;">AAA..AAA</span> et M = <span style="font-family: Consolas; font-size: 15px;">AA..A</span> constituent un **pire cas** pour l'algorithme de recherche naïve.

En effet, comme M est présent à chaque position de T, cela engendre $m$ comparaisons pour chacune de $n-m$ positions testées. Donc il y a en tout $(n-m)\times m$ comparaisons.

**Le coût (dans le pire cas) de l'algorithme de recherche naïve est de l'ordre de $(n-m)\times m$ (où $n$ et $m$ sont les tailles respectives du texte et du motif).**

## 3. Algorithme de Boyer-Moore-Horspool

???+ tip "Illustration de l'algorithme"

    ![](../../images/gif_BM.gif)

### 3.1 Principe


L'algorithme de Boyer-Moore-Horspool est toujours un algorithme de recherche par fenetre glissante :
 
- M "glisse" de gauche à droite le long de T   
- mais la comparaison M[0..m-1] vs T[i..i+m-1] se fait de droite à gauche    
- le décalage de M se fait de la façon suivante :  
    - si le motif est trouvé, on décale le motif d'un cran  
    - sinon, le décalage se calcule en fonction de ce qu'on appelle la règle du mauvais 
     caractère qui consiste :  
        - à aligner le premier caractère fautif du texte avec son occurrence la plus à droite dans M[0..m-2] (on ne prend pas en compte le dernier caractère du motif), sauf si celle-ci est déjà "passée", auquel cas on décale le motif d'un cran.  
        - si le motif ne contient pas le caractère fautif du texte, on décale le motif pour le positionner juste après la position fautive  

!!! example "Illustration"

    <iframe width="800" height="255" frameborder="0" src="https://germainbecker.github.io/recherche-textuelle/iframe-embed.html?r=bmh&t=GGCAGCCGAACCGCAGCAGCAC&m=GCAG"></iframe>

### 3.2 Prétraitement du motif M

Pour mettre en oeuvre la règle du mauvais caractère, il est nécessaire de **prétraiter le motif**, c'est-à-dire de l'analyser au préalable (avant de lancer la recherche) pour déterminer le décalage maximum possible selon les cas.

En pratique, on construit un tableau associatif `MC` (un dictionnaire) qui associe à chaque caractère $c$ du motif M, le nombre de position à "remonter" dans M pour trouver $c$. Autrement dit, la distance entre l'occurrence la plus à droite de chaque caractère et le dernier caractère du motif.

**Cas particulier** : si $c$ est le caractère en dernière position du motif, on cherche son avant-dernière occurrence (en partant de la droite bien sûr)

**Exemple** : Pour le motif M = GCAG, on obtient le tableau suivant

| Caractère $c$ | A | C | G |
| --- | --- | --- | --- |
| MC[$c$] | 1 | 2 | 3 |

### 3.3 Implémentation

```python
def table_bm(mot : str) -> dict :
    """ construit la table de décalages de Boyer-Moore"""
    table = {}
    for j in range(len(mot) - 1):
        table[mot[j]] = len(mot) - 1 - j
    return table

def rechercheBMH(M : str, T : str) -> list:
    # Prétraitement (règle du mauvais caractère)
    MC = table_MC(M)
    
    # Recherche par fenêtre glissante
    n = len(T)
    m = len(M)
    positions = []
    i = 0
    while i <= n - m:
        j = m - 1  # comparaison à partir de la droite !
        while j >= 0 and M[j] == T[i + j]:
            j = j - 1
        if j < 0 :  # si motif trouvé
            positions.append(i)
            i = i + 1  # décalage de 1
        else:  # si motif non trouvé --> caractère fautif : T[i+j]
            if T[i+j] in MC:  # si le caractère fautif T[i+j] est dans le motif
                i = i +  MC[T[i+j]] # on fait le décalage en fonction de MC
            else:  # si le caractère fautif T[i+j] n'est pas dans le motif
                i = i + m  # on place le motif juste après la position fautive
    return positions
```
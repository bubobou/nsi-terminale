---
hide:
  - footer
---

# Diviser pour régner⚓

## 1. Retour sur la recherche dichotomique

L'algorithme de recherche dichotomique (du grec dikhotomia, « division en deux parties ») est
étudié en classe de première, c'est un exemples d'application du principe « diviser pour régner».

On rappelle qu'il s'agit de déterminer si un entier *v* apparaît dans un
tableau *t*, ce dernier étant supposé trié. 

L'idée principale de la recherche dichotomique consiste à délimiter une
portion du tableau dans laquelle la valeur *v* peut encore se trouver, avec
deux indices *g* et *d*.


### 1.1 Version impérative

```python
def recherche_dichotomique(tab, val) :
    '''
    renvoie True ou False suivant la présence de la valeur val dans le tableau trié tab.
    '''
    i_debut = 0
    i_fin = len(tab) - 1
    while i_debut <= i_fin :
        i_centre = (i_debut + i_fin) // 2     # 
        val_centrale = tab[i_centre]          #  
        if val_centrale == val:               #  
            return True
        if val_centrale < val:                #  
            i_debut = i_centre + 1            #  
        else :
            i_fin = i_centre - 1
    return False
```

À chaque tour de la boucle `while`, la taille de la liste est divisée par 2. Ceci confère à cet algorithme une complexité logarithmique (bien meilleure qu'une complexité linéaire).

### 1.2 Version récursive

!!! note "avec les indices"

    Il est possible de programmer de manière récursive la recherche dichotomique sans toucher à la liste, et donc en jouant uniquement sur les indices :

    ```python
    def dicho_rec(tab, val, i=0, j=None): # 
        if j is None:                       # 
            j = len(tab)-1
        if i > j :
            return False
        m = (i + j) // 2
        if tab[m] < val :
            return dicho_rec(tab, val, m + 1, j)
        elif tab[m] > val :
            return dicho_rec(tab, val, i, m - 1 )
        else :
            return True
    ```

    Vous pouvez utiliser l'IDE pour tester cette fonction en cherchant d'abord 12, puis 17
    dans le tableau :

    ```tab = [1, 5, 7, 9, 12, 13]```

    {{ IDEv ()}}

!!! note "avec le slicing"

    Pour écrire simplement la version récursive de cet algorithme, nous pouvons aussi utiliser le slicing (découpage) de listes. Cette manipulation n'est pas au programme de NSI (même si elle est très simple). Attention, elle a un coût algorithmique important, qui peut fausser notre analyse de complexité.

    ```python
    def dichotomie_rec(tab, val):
        if len(tab) == 0:
            return False
        i_centre = len(tab) // 2
        if tab[i_centre] == val:
            return True
        if tab[i_centre] < val:
            return dichotomie_rec(tab[i_centre + 1:], val) # 
        else:
            return dichotomie_rec(tab[:i_centre], val)  # 
    ```

## 2. Diviser pour régner

Les algorithmes de dichotomie présentés ci-dessous ont tous en commun de diviser par 
deux la taille des données de travail à chaque étape. Cette méthode de résolution 
d'un problème est connue sous le nom de *diviser pour régner*, ou *divide and conquer* 
en anglais.


!!! note "Principe"

    Le principe «diviser pour régner» consiste à :

    - (1) **Diviser** : décomposer un problème en un ou plusieurs sous-problèmes 
     de même  nature, mais plus petits; 
    - (2) **Régner** : résoudre ces sous-problèmes, éventuellement en les décomposant 
     à leur tour récursivement en problèmes plus  petits encore; 
    - (3) **Combiner** : déduire des solutions des sous-problèmes la solution du 
     problème initial.
  
!!! example "Recherche dichotomique"

        - **Diviser** : comparer l’élément recherché  à celui  au milieu du
        tableau pour savoir s’il faut chercher à gauche ou à droite  
        - **Régner** : conclure si e=x, ou rechercher récursivement dans l’un
        des deux sous-tableaux  
        - **Combiner** : rien à faire. . .


Le paradigme diviser pour régner va naturellement amener à rédiger des programmes récursifs.

## 3. Le tri fusion

### 3.1 Principe

- **Diviser** : diviser le tableau en deux sous-tableaux de longueurs presque égales  
- **Régner** : trier récursivement chacun des deux sous-tableaux  
- **Combiner** : fusionner les deux sous-tableaux triés en temps linéaire  

!!! example "Exemple"

    Illustrons le principe du tri-fusion avec le tableau :
    
    [3, 16, 14, 1, 12, 7, 10, 4, 5, 11, 15]

    On va diviser le problème en deux sous-problèmes de la même forme : 
    ici, il suﬀit de couper le  tableau en deux, pour obtenir deux sous-tableaux 
    [3, 16, 14, 1, 12, 7] et [10, 4, 5, 11, 15] de taille  (presque) identique. 
    
    Imaginons qu’on sache trier ces deux sous-tableaux : on obtient alors les
    tableaux [1, 3, 7, 12, 14, 16] et [4, 5, 10, 11, 15]. Pour obtenir le grand 
    tableau trié, il suﬀit donc de fusionner ces deux tableaux triés.

    La question reste cependant entière : comment fait-on pour trier les deux 
    petits tableaux [3, 16, 14, 1, 12, 7] et [10, 4, 5, 11, 15] ?
    
    La réponse est simple : « on recommence ! ». En effet, la tactique décrite 
    ci-dessus pour trier le grand tableau peut être aussi utilisée pour traiter ces
    deux petits tableaux, de manière indépendante.

![exemple-tri](../../images/tri-fusion.png)

Le principe de l'algorithme du tri fusion est le suivant

- pour trier une liste, on fusionne les deux moitiés de cette liste, précédémment elles-mêmes triées par le tri fusion.  
- si une liste à trier est réduite à un élément, elle est déjà triée.

!!! tips "Algorithme du tri-fusion"

    ```
        fonction tri_fusion ( tableau ) :
            n <- longueur ( tableau )
            Si n ≤ 1 alors
                retourner ( tableau )
            Sinon
                gauche <- tableau [0 : n//2]
                droite <- tableau [n//2 + 1 : n−1 ]
                gauche_trié <- tri_fusion ( gauche )
                droite_trié <- tri_fusion ( droite )
                retourner ( fusionner ( gauche_trié , droite_trié ) )
            FinSi
    ```

    On a utilisé la notation tableau [0 : n//2] pour représenter la portion gauche du tableau
    constituée des cases d’indice 0, 1, . . . , n//2 du tableau.

    Contrairement au tri par insertion, cette fonction retourne un tableau : elle n’est pas en 
    place.
    
    On a également utilisé une fonction *fusionner* dont on ne fournit pas le pseudo-code : 
    c’est la fonction qui prend deux petits tableaux triés en entrée et doit les fusionner 
    pour produire un grand tableau trié, comme expliqué ci-dessus.


### 3.1 Étape : *Combiner*

Le mécanisme principal du tri fusion est la *fusion* de deux listes triées en une nouvelle liste elle aussi triée.

Ce mécanisme est aussi parfois désigné sous le terme d'*interclassement*.

Le principe de la fusion de deux listes `lst1` et `lst2`.

- on part d'une liste vide `lst_totale`
- on y ajoute alternativement les éléments de `lst1` et `lst2`. Il faut pour cela gérer séparément un indice `i1` pour la liste `lst1` et un indice `i2` pour la liste `lst2`.
- quand une liste est épuisée, on y ajoute la totalité restante de l'autre liste.

!!! example "Exercice"

    Coder en Python la fonction `fusion` qui prendra comme paramètre deux tableaux et qui
    renvoie le tableau issu de leur fusion par ordre croissant.

    {{ IDE()}}

### 3.2 Étapes *Diviser* et *Régner*

#### Principe

L'idée du tri fusion est le **découpage** de la liste originale en une multitude de listes ne contenant qu'**un seul élément**. 

Ces listes élémentaires seront ensuite **fusionnées** à l'aide de la fonction précédente.

![image](../../images/fusion.png){: .center}

 

#### Implémentation

La grande force de ce tri va être qu'il se programme simplement de manière récursive, en appelant à chaque étape la même fonction mais avec une taille de liste divisée par deux, ce qui justifie son classement parmi les algorithmes utilisants «diviser pour régner».

!!! note "Algorithme de tri fusion"

    ```python  

    def tri_fusion(lst):
        if len(lst) <= 1:
            return lst
        else:
            m = len(lst) // 2
            return fusion(tri_fusion(lst[:m]), tri_fusion(lst[m:]))
    ```

**Visualisation**

Une erreur classique avec les fonctions récursives est de considérer que les appels récursifs sont simultanés. Ceci est faux ! L'animation suivante montre la progression du tri :

![gifusion](../../images/gif_fusion.gif)

## 3.3 Complexité

La division par 2 de la taille de la liste pourrait nous amener à penser que le tri fusion est de complexité logarithmique, comme l'algorithme de dichotomie. Il n'en est rien.

En effet, l'instruction finale ```interclassement(tri_fusion(lst[:m]), tri_fusion(lst[m:]))``` lance **deux** appels à la fonction ```tri_fusion``` (avec certe des données d'entrée deux fois plus petites).

On peut montrer que :

!!! note "Complexité du tri fusion :heart:"
    L'algorithme de tri fusion est en $O(n \log n)$.

    On dit qu'il est **quasi-linéaire**. (ou *linéarithmique*)

Une complexité quasi-linéaire (en $O(n \log n)$) se situe «entre» une complexité linéaire (en $O(n)$) et une complexité quadratique (en $O(n^2)$). Mais elle est plus proche de la complexité linéaire.

![image](../../images/comparaison.png){: .center}


Une jolie animation permettant de comparer les tris :

![image](../../images/comparaisons.gif){: .center}


Issue de ce [site](https://www.toptal.com/developers/sorting-algorithms)


---

**Références**

  - Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.
  - [glassus.github.io](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.3_Arbres/cours/#54-insertion-dans-un-abr) G.LASSUS, Lycée François Mauriac - Bordeaux
  - Cours d'Introduction à l’informatique, Benjamin Monmege - Université Aix-Marseille

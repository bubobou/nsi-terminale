def rendu_monnaie(pieces, s):
    "renvoie le nombre minimale de pièces"
    if s == 0 :
        return 0
    r = s
    for p in pieces : 
        if p <= s : 
            r = min(r, 1 + rendu_monnaie(pieces, s-p))
    return r

r = rendu_monnaie([1,2,5,10,20],14)
print(r)
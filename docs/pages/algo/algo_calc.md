---
hide:
  - footer
---

# Caculabilité et décidabilité

## 1. Un programme est une donnée

Un programme est une simple donnée, pouvant être reçue en paramètre par un autre 
programme, voire par lui-même !

### 1.1 Interpréteur Python

Lorsque nous exécutons un programme python avec par exemple la commande `python prog.py`, 
on appelle la commande `python` qui est elle même un programme et on lui demande 
de s'exécuter avec comme paramètre `prog.py`.

Au passage on peut lancer la commande `python prog.py` par l'intermédiaire d'un terminal qui
est lui même un programme.

Le programme `python` prend `prog.py` en entrée et le lance en procédant au passage à 
des vérifications syntaxiques (indentation, nommage, etc.), puis exécute et affiche 
ce qui est demandé dans `prog.py` et s'arrête.

### 1.2 Programme de test

On a rencontré précédemment des méthodes permettant d'effectuer des tests sur nos 
programmes. Un programme de test comme par exemple `pytest.main` prend également en
entrée une liste de programmes et lance tous les tests qu'ils contiennent.

Là encore il s'agit de programme qui prennent comme entrée d'autres programmes.


### 1.3 Virus et anti-virus

Un virus est un programme qui vise à installer des codes malveillants et qui cherche 
à se répliquer et ainsi infecter d’autres fichiers ou machines. C’est donc un autre 
exemple de programme qui prend en en entrée des programmes pour en produire d’autres.

Un programme antivirus se lance sur un système d'exploitation (OS) et examine tous les 
fichiers susceptibles d'être infectés, notamment les fichiers exécutables. Là encore
il s'agit d'un programme qui prend en entrée d’autres programmes.

### 1.4 Interprétation et compilation

Un langage interprété est un langage dans lequel les instructions ne sont
pas directement exécutées par la machine cible, mais lues et exécutées une à
une par un programme externe (qui est normalement écrit dans le langage de la
machine cible). C'est le cas de Python.

Un langage compilé est un langage où le programme, une fois compilé, est
exprimé dans les instructions d’un langage d'assemblage de plus bas niveau
adapté à un système spécifique. C'est le cas du langage C. 

Que ce soit pour l'interprétation ou la compilation on constate là encore
qu'on utilise un prgogramme comme entrée d'un autre programme.

## 2. Problème de l'arrêt

Supposons qu'il existe une fonction Python `arret(prog,x)` qui termine toujours 
son exécution et renvoie `True` si la fonction Python `prog` s'arrête lorsqu'on 
lui passe en argument `x`. Nous pouvons construire une nouvelle fonction `decide(entree)` 
de la manière suivante :

```python
def decide(entree):
  if arret(entree, entree):
    while True:
      pass
  else:
    return True
```

On peut remarquer que le programme `decide` est appelé avec comme paramètres `entree, entree` 
, ce qui signifie que `entree` se prend lui-même en paramètre. On rappelle que ce n'est pas 
choquant, un code-source étant une donnée comme une autre.

Quel est le résultat de l'appel decide(decide) ? Cet appel s'arrête si et seulement si 
`arret(decide, decide)` renvoie `False` c’est-à-dire si et seulement si l'appel `decide(decide) `
ne s'arrête pas. Nous sommes donc face à une contradiction.

Nous venons de prouver que notre programme `arret`, censé prédire si un programme `prog` peut 
s'arrêter sur une entrée `x`, NE PEUT PAS EXISTER.

Ce résultat théorique, d'une importance cruciale, s'appelle le problème de l'arrêt.

!!! info "Problème de l'arrêt"

    Il ne peut pas exister de programme universel qui prendrait en entrées :

    un programme P
    une entrée E de ce programme P

    et qui déterminerait si ce programme P, lancé avec l'entrée E, va s'arrêter ou non. 



## 3. Calculabilité

Les ordinateurs peuvent-ils tout calculer ? Les pionniers de l'informatique
 théorique ont permis de répondre à cette question au début du 20e siècle.

En 1900, un des plus grands mathématiciens de l’époque, David Hilbert (1862-1943) énonce ce que l’on
appellera plus tard le « Programme de Hilbert », à savoir une liste des 23 plus grands problèmes qui, 
selon lui, restaient à résoudre en mathématiques.

![Hilbert source:wikipedia](https://upload.wikimedia.org/wikipedia/commons/7/79/Hilbert.jpg)

David Hilbert formula un nouveau problème en 1928 : le problème de la décision ou Entscheidungsproblem 
en allemand. La question est « Y a-t-il un algorithme qui, étant donné un système formel et une proposition 
logique dans ce système, pourra décider sans ambiguïté si cette proposition est vraie ou fausse
dans le système formel ? »

En 1936, Alonzo Church et Alan Turing montrent de manière indépendante que la réponse à cette 
question est négative. Pour sa démonstration, Alan Turing présente un modèle théorique de machine 
capable d'exécuter des instructions basiques sur un ruban infini, les machines de Turing.
Le mathématicien Alonzo Church démontre ce théorème de l'arrêt, mais par un moyen totalement différent, 
en inventant le lambda-calcul.


### 3.1 Notion de calculabilité⚓︎

Qu'y a-t-il derrière cette notion de calculabilité ? Cette notion, qui jette un pont entre les 
mathématiques (la vision de Church, pour schématiser) et l'informatique (la vision de Turing)
 n'est pas simple à définir !

Le calcul mathématique peut se réduire à une succession d'opérations élémentaires (songez à la multiplication 
entière comme une série d'additions). Les nombres calculables sont les nombres qui sont générables en un 
nombre fini d'opérations élémentaires. De la même manière, une fonction mathématique sera dite calculable 
s'il existe une suite finie d'opérations élémentaires permettant de passer d'un nombre $x$ à son image $f(x)$.

On retrouve cette notion d'opérations élémentaires dans les machines de Turing. Cette machine (théorique) permet 
de simuler tout ce qu'un programme informatique (une suite d'instructions) est capable d'exécuter. Un algorithme 
peut se réduire à une suite d'opérations élementaires, comme une fonction mathématique peut se réduire à une suite 
de calculs. Dès lors, on pourra considérer un algorithme comme une fonction.

Turing a démontré que l'ensemble des fonctions calculables, au sens de Church, était équivalent à l'ensemble des 
fonctions programmables sur sa machine. Certaines fonctions peuvent être calculables, ou ne pas l'être : c'est 
notamment le cas de notre fonction du problème de l'arrêt.

### 3.2 Langages Turing-complets⚓

Ce résultat ne dépend pas du langage utilisé : le fait que nous ayons utilisé Python au paragraphe précédent n'a pas 
d'influence sur notre démonstration. Nous savons depuis les machines de Turing que tous nos langages de programmation sont 
Turing-complets : ils sont tous capables de faire la même chose (avec plus ou moins de facilité !). Scratch, C, Python, 
*Java, Basic, Haskell, Brainfuck... tous ces langages sont théoriquement équivalents : la calculabilité ne dépend pas du 
langage utilisé.

### 3.3 (HP) Calculable, oui, mais facilement ? -> 1 million de $ à gagner ci-dessous.⚓

L'étude de la calculabilité d'une fonction (à prendre au sens le plus large, c'est-à-dire un algorithme) ne se limite pas à 
un choix binaire : «calculable» vs «non calculable».

Parmi les fonctions calculables, certaines peuvent l'être rapidement, et d'autre beaucoup moins.

On retrouve alors la notion bien connue de complexité algorithmique, qui permet de classifier les algorithmes suivant leur 
dépendance à la taille de leurs données d'entrée (voir le cours de Première).

On peut regrouper les problèmes suivant la complexité de l'algorithme qui permet de les résoudre.

#### 3.3.1 la classe P

!!! info "Définition de la classe P"

    On dira que sont de «classe P» tous les problèmes dont l'algorithme de recherche de solution est de complexité polynomiale.

Que retrouve-t-on dans la classe P ? Tous les problèmes dont la solution est un algorithme de complexité linéraire, 
quadratique, logarithmique... Tout mais surtout pas un algorithme de complexité exponentielle.

Pour le résumer très grossièrement, un problème de classe P est un problème que l'on sait résoudre en temps raisonnable 
(même grand).

  - le problème du tri d'une liste est dans P.
  - le problème de la factorisation d'un grand nombre (sur lequel repose la sécurité du RSA) n'est a priori pas dans P.
  - le problème de la primalité («ce nombre est-il premier ?») a longtemps été considéré comme n'étant pas dans P... jusqu'en 2002, où a été découvert le test de primalité AKS, de complexité polynomiale (d'ordre 6). Ce test est donc maintenant dans P.

#### 3.3.2 la classe NP

!!! info "Définition de la classe NP"

    On dira que sont de «classe NP» tous les problèmes dont l'algorithme de recherche de solution est 
    Non-déterministe Polynomial.

!!! warning " ⚠ NP ne signifie pas Non-Polynomial !!! ⚠"

    Que veut dire la formulation «non-déterministe polynomial» ? Cela fait référence à ce que serait capable de faire une 
    machine de Turing (donc, n'importe quel ordinateur) travaillant de manière non-déterministe, donc capable d'explorer 
    simultanément plusieurs solutions possibles. On peut imaginer un arbre dont le parcours se ferait simultanément dans 
    toutes les branches, et non en largeur ou profondeur comme nous l'avons vu.

Sur une machine non-déterministe, si la solution à un problème se trouve en temps polynomial, alors ce problème appartient
 à la classe NP.

Très bien, mais les machines non-déterministes... cela n'existe pas réellement. Comment caractériser concrètement cette 
classe de problème ?

Si la solution peut être trouvée de manière polynomiale par une machine non-déterministe, une machine déterministe qui
 aurait de la chance en partant directement vers la bonne solution la trouverait elle aussi de manière polynomiale. On 
 simplifie souvent cela en disant «la vérification de la solution est polynomiale». Cela nous donnne cette définition plus 
 accessible de la classe NP :

!!! info "Définition (plus simple) de la classe NP"

    On dira que sont de «classe NP» tous les problèmes dont l'algorithme de vérification de solution est polynomial.

Pour le résumer très grossièrement, un problème de classe NP est un problème dont on sait vérifier facilement si une solution proposée marche ou pas :

  - la résolution d'un sudoku est dans NP : si quelqu'un vous montre un sudoku rempli, vous pouvez très rapidement lui dire si sa solution est valable ou pas.  
  - la factorisation d'un nombre est dans NP : si quelqu'un vous propose 4567*6037 comme décomposition de 27570979, vous pouvez très rapidement lui dire s'il a raison. (oui.)  
  - le problème du sac à dos (en version décisionnelle) est dans NP. Une proposition de butin peut facilement être examinée pour savoir si elle est possible ou non.
  - le problème du voyageur de commerce (ou TSP : Traveller Sales Problem), en version décisionnelle, est dans NP. Si on vous propose un trajet, vous pouvez facilement vérifier que sa longueur est (par exemple) inférieure à 150 km.


Malheureusement, aucun de ces problèmes cités n'a (à ce jour) d'algorithme de résolution meilleur qu'exponentiel...


#### 2.2.3 P = NP, ou pas ?

Tous les problèmes de P ont une solution qui peut être trouvée de manière polynomiale. Donc évidemment, la vérification de cette solution est aussi polynomiale. Donc tous les problèmes de P sont dans NP. On dit que P est inclus dans NP, que l'on écrit P ⊂ NP.

Voici une capture d'écran de l'excellente vidéo [Nos algorithmes pourraient-ils être BEAUCOUP plus rapides ? (P=NP ?)](https://www.youtube.com/watch?v=AgtOCNCejQ8) de l'excellent David Louapre :

[image](https://glassus.github.io/terminale_nsi/T2_Programmation/2.3_Calculabilite_Decidabilite/data/louapre.png)

On y retrouve (en vert) la classe P, qui contient les algorithmes de tri. En blanc, la classe NP, qui contient les problèmes de factorisation, du sudoku, du sac-à-dos...

Si quelqu'un trouve un jour un algorithme de polynomial de factorisation, alors le problème de factorisation viendra se ranger dans P. (accessoirement, le RSA sera sans doute détruit par cette découverte, sauf si l'ordre de complexité est très grand)

Mais certains de ces problèmes dans NP ont une propriété remarquable : la résolution polynomiale d'un seul d'entre eux 
ferait ramener la totalité des problèmes NP dans P. On dit que ces problèmes sont NP-complets (marqués en rouge ci-dessus) 
Concrètement, si vous trouvez une solution polynomiale de résolution du sudoku, vous entrainez avec lui dans P tous les 
autres problèmes NP, et vous aurez ainsi prouvé que P = NP. Accessoirement, vous gagnerez aussi le prix d'un million de 
dollars promis par la fondation Clay à qui tranchera cette question... 

Actuellement, à part le grand [Donald Knuth](https://fr.wikipedia.org/wiki/Donald_Knuth), la plupart des chercheurs qui 
travaillent à ce problème sont plutôt pessimistes, et pensent que P ≠ NP. Cela signifie qu'ils pensent que certains 
problèmes ne pourront jamais avoir une solution polynomiale.



---
!!! Abstract "Sources et bibliographie"

    - G.Lassus (lycée François Mauriac, Bordeaux), [Terminale NSI - Calculabilité Décidabilité](https://glassus.github.io/terminale_nsi/T2_Programmation/2.3_Calculabilite_Decidabilite/cours/)
    - Prépabac NSI, Terminale, G. CONNAN, V. PETROV, G. ROZSAVOLGYI, L. SIGNAC, éditions HATIER.
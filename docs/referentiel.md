!!! info "Contenus et capacités du référentiel"
    
    === "Bases de données"

        **Modèle relationnel**

        - [x] Identifier les concepts définissant le modèle relationnel :
            relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.

        **Bases de données relationnelles**

        - [x] Savoir distinguer la structure d’une base de données de son contenu.
        - [x] Repérer des anomalies dans le schéma d’une base de données.

        **Requêtes SQL**

        - [x] Identifier les composants d’une requête.
        - [x] Construire des requêtes d’interrogation à l’aide des clauses du langage SQL : SELECT, FROM, WHERE, JOIN.
        - [x] Construire des requêtes d’insertion et de mise à jour à l’aide de : UPDATE, INSERT, DELETE.

        **Systèmes de gestion**
        > pas à l'épreuve terminale

        - [ ] Identifier les services rendus par un système de gestion de bases de données relationnelles :
          - persistance des données,
          - gestion des accès concurrents,
          - efficacité de traitement des requêtes,
          - sécurisation des accès.

    === "Langages et programmation"

        **Récursivité**

        - [ ] Écrire un programme récursif
        - [ ] Analyser le fonctionnement d’un programme récursif

        **Modularité**

        - [ ] Utiliser des API (Application Programming Interface) ou des bibliothèques.
        - [ ] Exploiter leur documentation.
        - [ ] Créer des modules simples et les documenter.

        **Mise au point**

        - [ ] Dans la pratique de la programmation, savoir répondre aux causes typiques de bugs :
            - problèmes liés au typage, effets de bord non désirés,
            - débordements dans les tableaux,
            - instruction conditionnelle non exhaustive,
            - choix des inégalités,
            - comparaisons et calculs entre flottants,
            - mauvais nommage des variables, etc.

        **Calculabilité, décidabilité**
        > pas à l'épreuve terminale

        - [ ] Comprendre que tout programme est aussi une donnée.
        - [ ] Comprendre que la calculabilité ne dépend pas du langage de programmation utilisé.
        - [ ] Montrer, sans formalisme théorique, que le problème de l’arrêt est indécidable.

        **Paradigmes**
        > pas à l'épreuve terminale

        - [ ] Distinguer sur des exemples les paradigmes impératif, fonctionnel et objet.
        - [ ] Choisir le paradigme de programmation selon le champ d’application d’un programme.

        


    === "Structures de données"

        **Interface et implémentation**

        - [ ] Spécifier une structure de données par son interface.
        - [ ] Distinguer interface et implémentation.
        - [ ] Écrire plusieurs implémentations d’une même structure de données.

        **Programmation objet**

        - [ ] Écrire la définition d’une classe. 
        - [ ] Accéder aux attributs et méthodes d’une classe.

        **Structures linéaires**

        - [ ] Distinguer des structures par le jeu des méthodes qui les caractérisent.
        - [ ] Choisir une structure de données adaptée à la situation à modéliser.
        - [ ] Distinguer la recherche d’une valeur dans une liste et dans un dictionnaire.
        
        **Arbres**

        - [ ] Identifier des situations nécessitant une structure de données arborescente.
        - [ ] Évaluer quelques mesures des arbres binaires (taille, encadrement de la hauteur, etc.).

        **Graphes**
        > pas à l'épreuve terminale

        - [ ] Modéliser des situations sous forme de graphes.
        - [ ] Écrire les implémentations correspondantes d’un graphe : matrice d’adjacence, liste de successeurs/de prédécesseurs.
        - [ ] Passer d’une représentation à une autre.


    === "Architectures matérielles, OS et réseaux"

        **Systèmes sur puces**
        > pas à l'épreuve terminale

        - [ ] Identifier les principaux composants sur un schéma de circuit et les avantages de leur intégration en termes de vitesse et de consommation.

        **Gestion des processus**
        

        - [ ] Décrire la création d’un processus, l’ordonnancement de plusieurs processus par le système.
        - [ ] Mettre en évidence le risque de l’interblocage (deadlock).

        **Protocoles de routage**

        - [ ] Identifier, suivant le protocole de routage utilisé, la route empruntée par un paquet.

        **Sécurisation des communications**
        > pas à l'épreuve terminale

        - [ ] Décrire les principes de chiffrement symétrique (clef partagée) et asymétrique (avec clef privée/clef publique).
        - [ ] Décrire l’échange d’une clef symétrique en utilisant un protocole asymétrique pour sécuriser une communication HTTPS.


    === "Algorithmique"
    
        **Diviser pour régner**

        - [ ] Écrire un algorithme utilisant la méthode "diviser pour régner"

        **Algorithme sur les arbres binaires**

        - [ ] Calculer la taille et la hauteur d’un arbre.
        - [ ] Parcourir un arbre de différentes façons (ordres infixe, préfixe ou suffixe ; ordre en largeur d’abord).
        - [ ] Rechercher une clé dans un arbre de recherche, insérer une clé.

        ***Algorithme sur les graphes***
        > pas à l'épreuve terminale

        - [ ] Parcourir un graphe en profondeur d’abord, en largeur d’abord.
        - [ ] Repérer la présence d’un cycle dans un graphe.
        - [ ] Chercher un chemin dans un graphe.

        
        ***Programmation dynamique***
        > pas à l'épreuve terminale

        - [ ] Utiliser la programmation dynamique pour écrire un algorithme.

        ***Recherche textuelle***
        > pas à l'épreuve terminale

        - [ ] Étudier l’algorithme de Boyer-Moore pour la recherche d’un motif dans un texte.
# Numérique et Science Informatique - Classe de Terminale

Site pour l'enseignement de la NSI pour la classe de Terminale 
du Lycée Philippe de Girard

## Ressources

Les contenus présentés s'inspirent largement des formidables ressources que sont :

- [le cours de Gilles Lassus du Lycée François Mauriac (Bordeaux)](https://glassus.github.io/terminale_nsi/)
- [le site Informatique Lycée de David Roche](https://pixees.fr/informatiquelycee/)
- [le cours de M.Gouyou du lycée Marguerite de Valois](https://cgouygou.github.io/TNSI/)

et surtout l'ouvrage de référence :

- [l'ouvrage Numérique et Sciences Informatiques](https://www.editions-ellipses.fr/accueil/10445-20818-specialite-numerique-et-sciences-informatiques-lecons-avec-exercices-corriges-terminale-nouveaux-programmes-9782340038554.html#) de 
  Balabonski Thibaut, Conchon Sylvain, Filliâtre Jean-Christophe, Nguyen Kim aux éditions Ellipses
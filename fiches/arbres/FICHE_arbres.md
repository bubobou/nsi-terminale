---
title: "Structure de données : Arbres"
author: 
date: "2023-01-11"
subject: "Markdown"
keywords: [Markdown]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
...


## Exercice 1 : représenter

**Dessiner** tous les arbres binaires ayant respectivement 3 et 4 nœuds.

## Exercice 2 : dénombrer

Sachant qu'il y a 1 arbre binaire vide, 1 arbre binaire contenant
1 nœud, 2 arbres binaires contenant 2 nœuds, 5 arbres binaires contenant 3
nœuds et 14 arbres binaires contenant 4 nœuds, **calculer** le nombre d'arbres
binaires contenant 5 nœuds.

On ne cherchera pas à les construire tous, mais seulement à les dénombrer.

## Exercice 3 : écrire une méthode de classe

**Ajouter** à la classe Noeud vue en TP, une méthode `__eq__` permettant de tester 
l'égalité entre deux arbres binaires à l'aide de l'opération ==.

## Exercice 4 : écrire une fonction

**Écrire** une fonction `parfait(h)` qui reçoit en argument un entier `h` supérieur ou 
égal à zéro et renvoie un arbre binaire parfait de hauteur `h`.


## Exercice 5 : exécuter un algorithme

1. Appliquer les trois algorithmes de parcours en profondeur (préfixe, postfixe et infixe)
   depuis la racine des arbres binaires suivants, en montrant les valeurs aﬀichées.

2. Trouver un arbre binaire dont le parcours préfixe est A, B, C, D, E, F et le parcours 
infixe est C, B, E, D, F, A. (Attention, on cherche bien un seul arbre qui admet ces deux 
parcours à la fois !).

![](exercices-parcours-arbres.png)

3. Trouver deux arbres binaires différents dont le parcours préfixe est
A, B, D, C, E, G, F et le parcours postfixe est D, B, G, E, F, C, A.



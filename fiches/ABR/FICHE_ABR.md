---
title: "Structure de données : Arbres binaires de recherche"
author: 
date: "2023-01-17"
subject: "Markdown"
keywords: [Markdown]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
...


## Activité : annuaire

On souhaite stocker un annuaire téléphonique, la première solution proposée sera d'utiliser
un tableau trié.

### 1. Recherche dans un tableau trié

a) Quel sera l'algorithme le plus approprié pour rechercher un nom dans l'annuaire ?

b) Quel serait le coût de la recherche dans un annuaire de 60 millions de personnes ?

### 2. Ajout dans un tableau trié

Basile est un nouvel abonné qu’il nous faut ajouter dans l’annuaire.

![](abr_activite.png){width=70%}

Quel est le coût de l'ajout de Basile dans l'annuaire ?

### 3. Utiliser un arbre

Proposer une structure arborescente permettant de stoker un annuaire, dont la recherche
sera de même complexité qu'un tableau trié.

### 4. Ajout dans un arbre 

Quelles opérations doit-on réaliser pour ajouter un élément dans un arbre ?

Quel est le coût d'un tel ajout ?
